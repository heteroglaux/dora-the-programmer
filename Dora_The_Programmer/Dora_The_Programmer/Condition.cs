﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dora_The_Programmer
{
    class Condition//<T>
    {
        public string attribute1, attribute2;
        public int cond1, cond2;
        public string comparisson;

        public Condition()
        {
            cond1 = 1;
            cond2 = 1;
            comparisson = "==";
        }

        public Condition(Condition newCondition)
        {
            attribute1 = newCondition.attribute1;
            attribute2 = newCondition.attribute2;
            cond1 = newCondition.cond1;
            cond2 = newCondition.cond2;
            comparisson = newCondition.comparisson;
        }

        public bool isAvailible(Moving[] movOrder = null, int move = 0)
        {
            switch (attribute1)
            {
                case "itemsInBackpack":
                    cond1 = Attributes.itemsInBackpack;
                    this.cond1 = Attributes.itemsInBackpack;
                    break;
                case "nextCell":
                    cond1 = Form1.GetFieldArray[movOrder[move].CurrentCoords.Y + movOrder[move].Direction.Y][movOrder[move].CurrentCoords.X + movOrder[move].Direction.X] > 0 ? 1 : 0;
                    break;
                case "unknownItem":
                    cond1 = Form1.GetFieldArray[movOrder[move].CurrentCoords.Y + movOrder[move].Direction.Y][movOrder[move].CurrentCoords.X + movOrder[move].Direction.X] < -5 ? 1 : 0;
                    break;
                default:
                    cond1 = 1;
                    break;
            }
            switch (comparisson)
            {
                case "==":
                    return cond1 == (cond2);
                case "!=":
                    return cond1 != (cond2);
                case ">":
                    return cond1 > (cond2);
                case "<":
                    return cond1 < (cond2);
                case ">=":
                    return cond1 >= (cond2);
                case "<=":
                    return cond1 <= (cond2);
                default:
                    return false;
            }
        }
    }
}
