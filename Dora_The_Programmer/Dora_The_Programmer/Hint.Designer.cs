﻿namespace Dora_The_Programmer
{
    partial class Hint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Hint));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.numbLabel = new System.Windows.Forms.Label();
            this.character = new System.Windows.Forms.PictureBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textLabel = new System.Windows.Forms.Label();
            this.right = new System.Windows.Forms.PictureBox();
            this.left = new System.Windows.Forms.PictureBox();
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.bush1 = new System.Windows.Forms.PictureBox();
            this.bush4 = new System.Windows.Forms.PictureBox();
            this.bush3 = new System.Windows.Forms.PictureBox();
            this.bush2 = new System.Windows.Forms.PictureBox();
            this.timer4 = new System.Windows.Forms.Timer(this.components);
            this.star = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.items = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.character)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.right)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.left)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bush1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bush4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bush3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bush2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.star)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(124, 164);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(351, 301);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // numbLabel
            // 
            this.numbLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.numbLabel.AutoSize = true;
            this.numbLabel.Font = new System.Drawing.Font("Segoe Print", 21.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numbLabel.Location = new System.Drawing.Point(295, 14);
            this.numbLabel.Name = "numbLabel";
            this.numbLabel.Size = new System.Drawing.Size(205, 51);
            this.numbLabel.TabIndex = 1;
            this.numbLabel.Text = "Подсказка 1";
            // 
            // character
            // 
            this.character.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.character.BackColor = System.Drawing.Color.Transparent;
            this.character.Image = ((System.Drawing.Image)(resources.GetObject("character.Image")));
            this.character.Location = new System.Drawing.Point(224, 264);
            this.character.Name = "character";
            this.character.Size = new System.Drawing.Size(50, 50);
            this.character.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.character.TabIndex = 0;
            this.character.TabStop = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 75;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Interval = 75;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // textBox1
            // 
            this.textBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textBox1.BackColor = System.Drawing.Color.White;
            this.textBox1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBox1.Location = new System.Drawing.Point(481, 164);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(174, 96);
            this.textBox1.TabIndex = 2;
            // 
            // button1
            // 
            this.button1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.button1.Enabled = false;
            this.button1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(521, 266);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(99, 35);
            this.button1.TabIndex = 3;
            this.button1.Text = "Запуск!";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // textLabel
            // 
            this.textLabel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.textLabel.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textLabel.Location = new System.Drawing.Point(123, 56);
            this.textLabel.Name = "textLabel";
            this.textLabel.Size = new System.Drawing.Size(549, 105);
            this.textLabel.TabIndex = 4;
            this.textLabel.Text = "Чтобы дать Даше команду идти, выбери направление(вверх, вниз, вправо или влево) и" +
    " скажи, сколько шагов ей следует сделать. Команда будет выглядеть следующим обра" +
    "зом: \"вниз(5);\"";
            this.textLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // right
            // 
            this.right.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.right.Cursor = System.Windows.Forms.Cursors.Hand;
            this.right.Image = ((System.Drawing.Image)(resources.GetObject("right.Image")));
            this.right.Location = new System.Drawing.Point(694, 237);
            this.right.Name = "right";
            this.right.Size = new System.Drawing.Size(73, 50);
            this.right.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.right.TabIndex = 5;
            this.right.TabStop = false;
            this.right.Click += new System.EventHandler(this.right_Click);
            // 
            // left
            // 
            this.left.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.left.Cursor = System.Windows.Forms.Cursors.Hand;
            this.left.Image = ((System.Drawing.Image)(resources.GetObject("left.Image")));
            this.left.Location = new System.Drawing.Point(12, 237);
            this.left.Name = "left";
            this.left.Size = new System.Drawing.Size(73, 50);
            this.left.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.left.TabIndex = 6;
            this.left.TabStop = false;
            this.left.Visible = false;
            this.left.Click += new System.EventHandler(this.left_Click);
            // 
            // timer3
            // 
            this.timer3.Interval = 75;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // bush1
            // 
            this.bush1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bush1.BackColor = System.Drawing.Color.Green;
            this.bush1.Image = ((System.Drawing.Image)(resources.GetObject("bush1.Image")));
            this.bush1.Location = new System.Drawing.Point(274, 264);
            this.bush1.Name = "bush1";
            this.bush1.Size = new System.Drawing.Size(50, 50);
            this.bush1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bush1.TabIndex = 7;
            this.bush1.TabStop = false;
            this.bush1.Visible = false;
            // 
            // bush4
            // 
            this.bush4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bush4.BackColor = System.Drawing.Color.Green;
            this.bush4.Image = ((System.Drawing.Image)(resources.GetObject("bush4.Image")));
            this.bush4.Location = new System.Drawing.Point(223, 315);
            this.bush4.Name = "bush4";
            this.bush4.Size = new System.Drawing.Size(50, 50);
            this.bush4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bush4.TabIndex = 7;
            this.bush4.TabStop = false;
            this.bush4.Visible = false;
            // 
            // bush3
            // 
            this.bush3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bush3.BackColor = System.Drawing.Color.Green;
            this.bush3.Image = ((System.Drawing.Image)(resources.GetObject("bush3.Image")));
            this.bush3.Location = new System.Drawing.Point(274, 365);
            this.bush3.Name = "bush3";
            this.bush3.Size = new System.Drawing.Size(50, 50);
            this.bush3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bush3.TabIndex = 7;
            this.bush3.TabStop = false;
            this.bush3.Visible = false;
            // 
            // bush2
            // 
            this.bush2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.bush2.BackColor = System.Drawing.Color.Green;
            this.bush2.Image = ((System.Drawing.Image)(resources.GetObject("bush2.Image")));
            this.bush2.Location = new System.Drawing.Point(324, 315);
            this.bush2.Name = "bush2";
            this.bush2.Size = new System.Drawing.Size(50, 50);
            this.bush2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.bush2.TabIndex = 7;
            this.bush2.TabStop = false;
            this.bush2.Visible = false;
            // 
            // timer4
            // 
            this.timer4.Interval = 75;
            this.timer4.Tick += new System.EventHandler(this.timer4_Tick);
            // 
            // star
            // 
            this.star.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.star.BackColor = System.Drawing.Color.Transparent;
            this.star.Image = ((System.Drawing.Image)(resources.GetObject("star.Image")));
            this.star.Location = new System.Drawing.Point(324, 264);
            this.star.Name = "star";
            this.star.Size = new System.Drawing.Size(50, 50);
            this.star.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.star.TabIndex = 0;
            this.star.TabStop = false;
            this.star.Visible = false;
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(495, 346);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(208, 19);
            this.label2.TabIndex = 9;
            this.label2.Text = "Даша.вещей_в_рюкзаке   = ";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Visible = false;
            // 
            // items
            // 
            this.items.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.items.AutoSize = true;
            this.items.Font = new System.Drawing.Font("Times New Roman", 12.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.items.ForeColor = System.Drawing.Color.Maroon;
            this.items.Location = new System.Drawing.Point(701, 346);
            this.items.Name = "items";
            this.items.Size = new System.Drawing.Size(18, 19);
            this.items.TabIndex = 10;
            this.items.Text = "0";
            this.items.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.items.Visible = false;
            // 
            // Hint
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(779, 499);
            this.Controls.Add(this.items);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.character);
            this.Controls.Add(this.star);
            this.Controls.Add(this.bush2);
            this.Controls.Add(this.bush3);
            this.Controls.Add(this.bush4);
            this.Controls.Add(this.bush1);
            this.Controls.Add(this.left);
            this.Controls.Add(this.right);
            this.Controls.Add(this.textLabel);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.numbLabel);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Hint";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Подсказка";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.character)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.right)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.left)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bush1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bush4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bush3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bush2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.star)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label numbLabel;
        private System.Windows.Forms.PictureBox character;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label textLabel;
        private System.Windows.Forms.PictureBox right;
        private System.Windows.Forms.PictureBox left;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.PictureBox bush1;
        private System.Windows.Forms.PictureBox bush4;
        private System.Windows.Forms.PictureBox bush3;
        private System.Windows.Forms.PictureBox bush2;
        private System.Windows.Forms.Timer timer4;
        private System.Windows.Forms.PictureBox star;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label items;
    }
}