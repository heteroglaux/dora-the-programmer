﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Dora_The_Programmer
{
    class Moving
    {
        public string CharacterToMove;  
        public PictureBox ObjectToMove;
        public Point CoordsBeforeMoving;
        public Point CurrentCoords; // Координаты перед движением по прямой
        public Point CoordsAfterStep;
        public Point Direction = new Point(0, 0);
        public string Info;
        public Condition/*<object>*/ Condition = new Condition/*<object>*/();
        public bool WHILE = false;

        public static Moving commandToAdd = new Moving();

        public Moving() { }

        public Moving(PictureBox objectToMove)
        {
            ObjectToMove = objectToMove;
        }

        public Moving(Moving newMov)
        {
            CharacterToMove = newMov.CharacterToMove;
            ObjectToMove = newMov.ObjectToMove;
            CoordsBeforeMoving = newMov.CoordsBeforeMoving;
            CurrentCoords = newMov.CurrentCoords;
            CoordsAfterStep = newMov.CoordsAfterStep;
            Direction = newMov.Direction;
            Info = newMov.Info;
            Condition = new Condition(newMov.Condition);
            WHILE = newMov.WHILE;
        }

        public Moving(string characterToMove, PictureBox objectToMove, Point startLoc, Point direction, int stepSize, string info = "")
        {
            CharacterToMove = characterToMove;
            ObjectToMove = objectToMove;
            CurrentCoords = startLoc;
            CoordsAfterStep = new Point(startLoc.X + direction.X, startLoc.Y + direction.Y);
            CoordsBeforeMoving = new Point((objectToMove.Location.X - 30) / stepSize, (objectToMove.Location.Y - 30) / stepSize);
            Info = info;
        }
    }
}
