﻿namespace Dora_The_Programmer
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.GO = new System.Windows.Forms.Button();
            this.walkingTimer = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.shoe = new System.Windows.Forms.PictureBox();
            this.dora = new System.Windows.Forms.PictureBox();
            this.backToMap = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.item1 = new System.Windows.Forms.PictureBox();
            this.item2 = new System.Windows.Forms.PictureBox();
            this.item3 = new System.Windows.Forms.PictureBox();
            this.codeBox = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dora)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.item3)).BeginInit();
            this.SuspendLayout();
            // 
            // GO
            // 
            this.GO.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.GO.Location = new System.Drawing.Point(808, 236);
            this.GO.Name = "GO";
            this.GO.Size = new System.Drawing.Size(97, 65);
            this.GO.TabIndex = 2;
            this.GO.Text = "Запуск!";
            this.GO.UseVisualStyleBackColor = true;
            this.GO.Click += new System.EventHandler(this.GO_Click);
            // 
            // walkingTimer
            // 
            this.walkingTimer.Interval = 1;
            this.walkingTimer.Tick += new System.EventHandler(this.walkingTimer_Tick);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(808, 94);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(97, 65);
            this.button1.TabIndex = 3;
            this.button1.Text = "Получить подсказку";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(802, 720);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            // 
            // shoe
            // 
            this.shoe.BackColor = System.Drawing.Color.Transparent;
            this.shoe.Image = ((System.Drawing.Image)(resources.GetObject("shoe.Image")));
            this.shoe.Location = new System.Drawing.Point(42, 140);
            this.shoe.Name = "shoe";
            this.shoe.Size = new System.Drawing.Size(40, 40);
            this.shoe.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.shoe.TabIndex = 8;
            this.shoe.TabStop = false;
            this.shoe.Visible = false;
            this.shoe.Click += new System.EventHandler(this.shoe_Click);
            // 
            // dora
            // 
            this.dora.BackColor = System.Drawing.Color.Transparent;
            this.dora.Image = ((System.Drawing.Image)(resources.GetObject("dora.Image")));
            this.dora.Location = new System.Drawing.Point(51, 48);
            this.dora.Name = "dora";
            this.dora.Size = new System.Drawing.Size(40, 40);
            this.dora.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.dora.TabIndex = 9;
            this.dora.TabStop = false;
            this.dora.Visible = false;
            this.dora.Click += new System.EventHandler(this.dora_Click);
            // 
            // backToMap
            // 
            this.backToMap.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.backToMap.Location = new System.Drawing.Point(808, 165);
            this.backToMap.Name = "backToMap";
            this.backToMap.Size = new System.Drawing.Size(97, 65);
            this.backToMap.TabIndex = 11;
            this.backToMap.Text = "Назад на карту";
            this.backToMap.UseVisualStyleBackColor = true;
            this.backToMap.Click += new System.EventHandler(this.backToMap_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(511, 576);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(239, 95);
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            // 
            // item1
            // 
            this.item1.BackColor = System.Drawing.Color.Transparent;
            this.item1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.item1.Location = new System.Drawing.Point(597, 596);
            this.item1.Name = "item1";
            this.item1.Size = new System.Drawing.Size(40, 40);
            this.item1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.item1.TabIndex = 13;
            this.item1.TabStop = false;
            // 
            // item2
            // 
            this.item2.BackColor = System.Drawing.Color.Transparent;
            this.item2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.item2.Location = new System.Drawing.Point(648, 611);
            this.item2.Name = "item2";
            this.item2.Size = new System.Drawing.Size(40, 40);
            this.item2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.item2.TabIndex = 14;
            this.item2.TabStop = false;
            // 
            // item3
            // 
            this.item3.BackColor = System.Drawing.Color.Transparent;
            this.item3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.item3.Location = new System.Drawing.Point(699, 600);
            this.item3.Name = "item3";
            this.item3.Size = new System.Drawing.Size(40, 40);
            this.item3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.item3.TabIndex = 15;
            this.item3.TabStop = false;
            // 
            // codeBox
            // 
            this.codeBox.AutoWordSelection = true;
            this.codeBox.DetectUrls = false;
            this.codeBox.Font = new System.Drawing.Font("Comic Sans MS", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.codeBox.Location = new System.Drawing.Point(510, 70);
            this.codeBox.Name = "codeBox";
            this.codeBox.Size = new System.Drawing.Size(240, 506);
            this.codeBox.TabIndex = 16;
            this.codeBox.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FloralWhite;
            this.ClientSize = new System.Drawing.Size(922, 732);
            this.Controls.Add(this.codeBox);
            this.Controls.Add(this.item3);
            this.Controls.Add(this.item2);
            this.Controls.Add(this.item1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.backToMap);
            this.Controls.Add(this.shoe);
            this.Controls.Add(this.dora);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.GO);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(938, 770);
            this.MinimumSize = new System.Drawing.Size(938, 770);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Игровое поле";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.shoe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dora)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.item3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button GO;
        private System.Windows.Forms.Timer walkingTimer;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox shoe;
        private System.Windows.Forms.PictureBox dora;
        private System.Windows.Forms.Button backToMap;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox item1;
        private System.Windows.Forms.PictureBox item2;
        private System.Windows.Forms.PictureBox item3;
        private System.Windows.Forms.RichTextBox codeBox;

    }
}

