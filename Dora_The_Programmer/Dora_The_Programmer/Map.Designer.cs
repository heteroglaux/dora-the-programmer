﻿namespace Dora_The_Programmer
{
    partial class Map
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Map));
            this.flowerTimer = new System.Windows.Forms.Timer(this.components);
            this.level5 = new System.Windows.Forms.PictureBox();
            this.level4 = new System.Windows.Forms.PictureBox();
            this.level3 = new System.Windows.Forms.PictureBox();
            this.level2 = new System.Windows.Forms.PictureBox();
            this.level1 = new System.Windows.Forms.PictureBox();
            this.mapBackButton = new System.Windows.Forms.Panel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.level5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.level4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.level3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.level2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.level1)).BeginInit();
            this.SuspendLayout();
            // 
            // flowerTimer
            // 
            this.flowerTimer.Interval = 20;
            this.flowerTimer.Tick += new System.EventHandler(this.flowerTimer_Tick);
            // 
            // level5
            // 
            this.level5.BackColor = System.Drawing.Color.Transparent;
            this.level5.Location = new System.Drawing.Point(818, 233);
            this.level5.Name = "level5";
            this.level5.Size = new System.Drawing.Size(95, 125);
            this.level5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.level5.TabIndex = 6;
            this.level5.TabStop = false;
            // 
            // level4
            // 
            this.level4.BackColor = System.Drawing.Color.Transparent;
            this.level4.Location = new System.Drawing.Point(651, 266);
            this.level4.Name = "level4";
            this.level4.Size = new System.Drawing.Size(95, 125);
            this.level4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.level4.TabIndex = 7;
            this.level4.TabStop = false;
            // 
            // level3
            // 
            this.level3.BackColor = System.Drawing.Color.Transparent;
            this.level3.Location = new System.Drawing.Point(471, 277);
            this.level3.Name = "level3";
            this.level3.Size = new System.Drawing.Size(95, 125);
            this.level3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.level3.TabIndex = 8;
            this.level3.TabStop = false;
            // 
            // level2
            // 
            this.level2.BackColor = System.Drawing.Color.Transparent;
            this.level2.Location = new System.Drawing.Point(300, 288);
            this.level2.Name = "level2";
            this.level2.Size = new System.Drawing.Size(95, 125);
            this.level2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.level2.TabIndex = 9;
            this.level2.TabStop = false;
            // 
            // level1
            // 
            this.level1.BackColor = System.Drawing.Color.Transparent;
            this.level1.Location = new System.Drawing.Point(136, 349);
            this.level1.Name = "level1";
            this.level1.Size = new System.Drawing.Size(95, 125);
            this.level1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.level1.TabIndex = 10;
            this.level1.TabStop = false;
            // 
            // mapBackButton
            // 
            this.mapBackButton.BackColor = System.Drawing.Color.Transparent;
            this.mapBackButton.Location = new System.Drawing.Point(91, 46);
            this.mapBackButton.Name = "mapBackButton";
            this.mapBackButton.Size = new System.Drawing.Size(103, 150);
            this.mapBackButton.TabIndex = 12;
            this.mapBackButton.MouseClick += new System.Windows.Forms.MouseEventHandler(this.mapBackButton_MouseClick);
            this.mapBackButton.MouseMove += new System.Windows.Forms.MouseEventHandler(this.mapBackButton_MouseMove);
            // 
            // toolTip1
            // 
            this.toolTip1.ForeColor = System.Drawing.Color.Indigo;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.UseAnimation = false;
            // 
            // Map
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1049, 536);
            this.Controls.Add(this.mapBackButton);
            this.Controls.Add(this.level5);
            this.Controls.Add(this.level4);
            this.Controls.Add(this.level3);
            this.Controls.Add(this.level2);
            this.Controls.Add(this.level1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Map";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Карта";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Map_FormClosing);
            this.Load += new System.EventHandler(this.Map_Load);
            this.Shown += new System.EventHandler(this.Map_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.level5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.level4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.level3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.level2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.level1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer flowerTimer;
        private System.Windows.Forms.PictureBox level5;
        private System.Windows.Forms.PictureBox level4;
        private System.Windows.Forms.PictureBox level3;
        private System.Windows.Forms.PictureBox level2;
        private System.Windows.Forms.PictureBox level1;
        private System.Windows.Forms.Panel mapBackButton;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}