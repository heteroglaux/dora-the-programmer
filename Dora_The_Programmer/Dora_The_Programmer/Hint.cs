﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dora_The_Programmer
{
    public partial class Hint : Form
    {
        PictureBox[] bushes;
        int currentHint = 0;
        int maxHint;
        public Hint(int loc, int lev)
        {
            InitializeComponent();
            bushes = new PictureBox[] { bush1, bush2, bush3, bush4 };
            character.BackColor = Color.FromArgb(233, 222, 150);
            star.BackColor = Color.FromArgb(233, 222, 150);
            int n = loc * 10 + lev;
            if (n < 16)
            {
                timer1.Enabled = true;
                maxHint = 1;
            }
            else if (n < 26)
            {
                maxHint = 2;
            }
            else if (n < 33)
            {
                timer1.Enabled = true;
                maxHint = 4;
            }
            else if (n < 36)
            {
                maxHint = 5;
            }
            else if (n < 46)
            {
                maxHint = 6;
            }
            else
            {
                maxHint = 6;
            }
            Set();
        }

        int i = 0, iter = 0, move = 0, t;
        string[][] methods = new string[][] {
            new string[] { "вправо(3);", "вверх(1);", "влево(3);", "вниз(1);" },
            new string[] { "вправо(3);\nвверх(1);\nвлево(3);\nвниз(1);" },
            new string[] { "влево(-3);", "вниз(-1);", "вправо(-3);", "вверх(-1);" },
            new string[] { "Башмачок.вправо(3);", "Башмачок.вверх(1);", "Башмачок.влево(3);", "Башмачок.вниз(1);" },
            new string[] { "Даша.вправо(3);", "Даша.вверх(1);", "Даша.влево(3);", "Даша.вниз(1);" },
            new string[] { "Башмачок.прыжок(вправо);", "Башмачок.прыжок(вниз);", "Башмачок.прыжок(влево);", "Башмачок.прыжок(вверх);" },
            new string[] { "вправо(3); взять(); влево(3);" }
        };
        string[] texts = {
                             "Чтобы дать Даше команду идти, выбери направление(вверх, вниз, вправо или влево) и скажи, сколько шагов ей следует сделать. Команда будет выглядеть следующим образом: \"вниз(5);\"",
                             "Можно писать несколько команд подряд, а потом нажать кнопку \"Запуск!\". Даша выполнит все твои команды по очереди.",
                             "Ты можешь использовать отрицательные числа. Если написать \"вниз(-5);\", то Даша пройдёт на 5 клеток вверх.",
                             "Ты можешь давать команды Башмачку. Для этого в начале команды напиши его имя, а после точки саму команду. Это будет выглядеть следующим образом: \"Башмачок.вниз(4);\"",
                             "Ты можешь обращаться к Даше по имени. Команда Даша.вверх(1); и команда вверх(1); равнозначны.",
                             "Башмачок может прыгать. Чтобы он прыгнул, подведи его к препятствию, дай команду прыжка и направление, в котором он должен прыгать: \"Башмачок.прыжок(вниз);\".",
                             "Чтобы Даша могла подобрать звёздочку, дай ей команду сначала встать на клеточку со звездой. После этого введи команду \"взять\", тогда она положит звёздочку в рюкзак, а количество вещей в рюкзаке изменится."
                         };
        Point[] dir = new Point[] { new Point(1, 0), new Point(0, -1), new Point(-1, 0), new Point(0, 1) };
        Point[] dir1 = new Point[] { new Point(1, 0), new Point(0, 1), new Point(-1, 0), new Point(0, -1) };
        Point[] dir2 = new Point[] { new Point(1, 0), new Point(-1, 0) };
        Color[] buttonColor = new Color[] { Color.Teal, Color.DarkSlateGray, Color.Teal, Color.Transparent };
        private void timer1_Tick(object sender, EventArgs e)
        {
            switch (timer1.Interval)
            {
                case 75:
                    if (i < methods[currentHint][iter % 4].Length)
                        textBox1.Text += methods[currentHint][iter % 4][i++];
                    else
                    {
                        i = 0;
                        timer1.Interval = 400;
                    }
                    break;
                case 400:
                    timer1.Interval = 60;
                    break;
                case 60:
                    if (i < buttonColor.Length)
                        button1.BackColor = buttonColor[i++];
                    else
                    {
                        i = 0;
                        timer1.Interval = 1;
                        t = (dir[iter % 4].X == 0) ? 25 : 75;
                    }
                    break;
                case 1:
                    if (move++ < t)
                        character.Location = new Point(character.Location.X + dir[iter % 4].X * 2, character.Location.Y + dir[iter % 4].Y * 2);
                    else
                    {
                        textBox1.Text = "";
                        move = 0;
                        iter++;
                        timer1.Interval = 75;
                    }
                    break;
            }
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            switch (timer2.Interval)
            {
                case 75:
                    if (i < methods[currentHint][0].Length)
                        textBox1.Text += methods[currentHint][0][i++];
                    else
                    {
                        i = 0;
                        timer2.Interval = 400;
                    }
                    break;
                case 400:
                    timer2.Interval = 60;
                    break;
                case 60:
                    if (i < buttonColor.Length)
                        button1.BackColor = buttonColor[i++];
                    else
                    {
                        i = 0;
                        timer2.Interval = 1;
                        t = (dir[iter % 2].X == 0) ? 50 : 150;
                    }
                    break;
                case 1:
                    if (move++ < t)
                        character.Location = new Point(character.Location.X + dir[iter % 4].X, character.Location.Y + dir[iter % 4].Y);
                    else
                    {
                        move = 0;
                        iter++;
                        t = t == 50 ? 150 : 50;
                        if (character.Location == new Point(224, 264))
                        {
                            textBox1.Text = "";
                            timer2.Interval = 75;
                        }
                    }
                    break;
            }
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            switch (timer3.Interval)
            {
                case 75:
                    if (i < methods[currentHint][iter % 4].Length)
                        textBox1.Text += methods[currentHint][iter % 4][i++];
                    else
                    {
                        i = 0;
                        timer3.Interval = 400;
                    }
                    break;
                case 400:
                    timer3.Interval = 60;
                    break;
                case 60:
                    if (i < buttonColor.Length)
                        button1.BackColor = buttonColor[i++];
                    else
                    {
                        i = 0;
                        timer3.Interval = 1;
                        t = 100;
                    }
                    break;
                case 1:
                    if (move++ < t)
                        character.Location = new Point(character.Location.X + dir1[iter % 4].X, character.Location.Y + dir1[iter % 4].Y);
                    else
                    {
                        move = 0;
                        iter++;
                        textBox1.Text = "";
                        t = 100;
                        timer3.Interval = 75;
                    }
                    break;
            }
        }

        private void timer4_Tick(object sender, EventArgs e)
        {
            switch (timer4.Interval)
            {
                case 75:
                    if (i < methods[currentHint][0].Length)
                        textBox1.Text += methods[currentHint][0][i++];
                    else
                    {
                        i = 0;
                        timer4.Interval = 400;
                    }
                    break;
                case 400:
                    timer4.Interval = 60;
                    break;
                case 60:
                    if (i < buttonColor.Length)
                        button1.BackColor = buttonColor[i++];
                    else
                    {
                        i = 0;
                        timer4.Interval = 1;
                        t = 100;
                    }
                    break;
                case 1:
                    if (move++ < t)
                        character.Location = new Point(character.Location.X + dir2[iter % 2].X, character.Location.Y);
                    else
                    {
                        star.Visible = false;
                        move = 0;
                        iter++;
                        t = 100;
                        items.Text = "1";
                        if (character.Location == new Point(224, 264))
                        {
                            star.Visible = true;
                            textBox1.Text = "";
                            timer4.Interval = 75;
                            items.Text = "0";
                        }
                    }
                    break;
            }
        }

        private void right_Click(object sender, EventArgs e)
        {
            try
            {
                currentHint++;
                Set();
            }
            catch
            {
                currentHint = maxHint - 1;
                right.Visible = false;
            }
        }

        private void Set()
        {
            right.Visible = currentHint + 1 <= maxHint;
            left.Visible = currentHint != 0;
            textLabel.Text = texts[currentHint];
            numbLabel.Text = "Подсказка " + (currentHint + 1) + "/" + (maxHint + 1);
            i = 0; iter = 0; move = 0; t = 0;
            button1.BackColor = buttonColor[3];
            textBox1.Text = "";
            character.Location = new Point(224, 264);
            switch (currentHint)
            {
                case 0:
                    timer1.Interval = 75;
                    timer1.Enabled = true;
                    timer2.Enabled = false;
                    timer3.Enabled = false;
                    timer4.Enabled = false;
                    character.Image = Image.FromFile("characters/1.png");
                    break;
                case 1:
                    timer2.Interval = 75;
                    timer1.Enabled = false;
                    timer2.Enabled = true;
                    timer3.Enabled = false;
                    timer4.Enabled = false;
                    character.Image = Image.FromFile("characters/1.png");
                    break;
                case 2:
                    timer1.Interval = 75;
                    timer1.Enabled = true;
                    timer2.Enabled = false;
                    timer3.Enabled = false;
                    timer4.Enabled = false;
                    character.Image = Image.FromFile("characters/1.png");
                    break;
                case 3:
                    timer1.Interval = 75;
                    timer1.Enabled = true;
                    timer2.Enabled = false;
                    timer3.Enabled = false;
                    timer4.Enabled = false;
                    character.Image = Image.FromFile("characters/m1.png");
                    break;
                case 4:
                    timer1.Interval = 75;
                    timer1.Enabled = true;
                    timer2.Enabled = false;
                    timer3.Enabled = false;
                    timer4.Enabled = false;
                    character.Image = Image.FromFile("characters/1.png");
                    foreach (PictureBox pb in bushes)
                        pb.Visible = false;
                    break;
                case 5:
                    timer3.Interval = 75;
                    timer1.Enabled = false;
                    timer2.Enabled = false;
                    timer3.Enabled = true;
                    timer4.Enabled = false;
                    character.Image = Image.FromFile("characters/m1.png");
                    foreach (PictureBox pb in bushes)
                        pb.Visible = true;
                    star.Visible = false;
                    label2.Visible = false;
                    items.Visible = false;
                    break;
                case 6:
                    timer4.Interval = 75;
                    timer1.Enabled = false;
                    timer2.Enabled = false;
                    timer3.Enabled = false;
                    timer4.Enabled = true;
                    character.Image = Image.FromFile("characters/1.png");
                    foreach (PictureBox pb in bushes)
                        pb.Visible = false;
                    star.Visible = true;
                    label2.Visible = true;
                    items.Visible = true;
                    break;
            }
        }

        private void left_Click(object sender, EventArgs e)
        {
            try
            {
                currentHint--;
                Set();
            }
            catch
            {
                currentHint = 0;
                left.Visible = false;
            }
        }
    }
}
