﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.IO;

namespace Dora_The_Programmer
{

    public partial class Form1 : Form
    {
        #region Переменные
        //bool readingIfWhileNow = false;
        bool isMovingNow = false;
        bool b = true; // Использую для запоминания начальных кокординат Даши и Башмака, делаю фолс после первого прочтения из файла (чтобы при перерисовке формы, координаты не сбивались
        const int d = 40; // Высота, ширина квадратика поля
        int levelNow, locationNow; // Текущий уровень, текущая локация
        int starCount; // Количество звезд за уровень (заполняется в вокингТаймер)
        string charToMove; // Название персонажа, который сейчас будет двигаться
        static int[][] p = new int[17][]; // Массив, считанный из файла с полем
        public static int[][] GetFieldArray { get { return p; } }
        int[,] strCount = {
                              { 1, 2, 4, 5, 7 },
                              { 7, 3, 3, 5, 5 },
                              { 3, 3, 4, 4, 6 },
                              { 1, 1, 1, 1, 1 },
                              { 1, 1, 1, 1, 1 },
                              { 1, 1, 1, 1, 1 },
                              { 1, 1, 1, 1, 1 },
                              { 1, 1, 1, 1, 1 },
                              { 1, 1, 1, 1, 1 },
                              { 1, 1, 1, 1, 1 }
                          };
        string[] messages =
        {
            "\nДаша не понимает твоeй команды...\nПопробуй попросить что-нибудь другое.\n",
            "\nОй! Впереди преграда!..\nВыбери другое направление или задай меньшее количество шагов.\n",
            "\nЧто-то пошло не так...\nУбедись, что ты правильно расставил скобки.\n",
            "\nЧто-то пошло не так...\nПравильно ли ты написал число?\n",
            "\n\nПоздравляю! Ты спас Башмачка!\nТеперь ты можешь перейти к\nследующей задачке.",
            // 5
            "\nЧто-то пошло не так...\nТы не забыл поставить \";\" в конце команды?\n",
            "Даша не умеет прыгать так хорошо, как Башмачок...\nПопробуй скомандовать обезьянке прыгнуть!",
            "\nОй! Впереди преграда, Башмачок не может прыгнуть... Выбери другое направление прыжка или попробуй ее обойти.",
            "\nБашмачок не знает, куда ему прыгать... Правильно ли ты задал направление прыжка?",
            "\nК кому ты обращаешься? Попробуй скомандовать что-то Даше или Башмачку.",
            //10
            "\nТы не забыл поставить точку после обращения к кому-то?",
            "\nБашмачок не понимает твоeй команды...\nПопробуй попросить что-нибудь другое.\n",
            "\nТы должен обязательно дать какое-нибудь значение этому свойству или переменной!",
            "\nУ Даши нет такого свойства... Попробуй написать что-нибудь другое или получить информацию о Даше.",
            "\nТебе нужно сравнить это значение с чем-то еще!",
            //15
            "\nТы можешь только узнать, равно ли первое значение другому",
            "\nУ Башмачка нет такого свойства... Попробуй написать что-нибудь другое или получить информацию об обезьянке.",
            "\nОй!.. Даша не нашла Башмачка... Не огорчайся, попробуй еще раз! =)",
            "\nДаша не может взять звездочку, которой там нет... Найди звезду, и только тогда пиши команду \"взять\".",
            "\nОй!.. У тебя где-то бесконечный цикл! Обязательно укажи направление движения после \"пока\".",
            //20
            "\nОсторожно! Ядовитый гриб! Даша не должна брать мухоморы, впредь будь повнимательнее!",
            "\nДаша не видит неизвестных предметов... Стань на один из них, а потом обращайся к нему!"
        }; // Сообщения, передаваемые форме ошибки/поздравления
        Point picBoxNow = new Point(); // Координаты картинки, которую я буду двигать
        Point fieldLocation = new Point(30, 30); // Координаты верхнего левого угла поля (для отрисовки)
        Color[] backColors = { Color.FromArgb(233, 222, 150), Color.FromArgb(242, 254, 253), Color.FromArgb(248, 177, 50), Color.FromArgb(250, 197, 52), Color.FromArgb(243, 254, 253), Color.FromArgb(248, 177, 50), Color.FromArgb(254, 208, 91), Color.FromArgb(153, 217, 234), Color.FromArgb(248, 177, 50), Color.FromArgb(204, 228, 25) };
        Image[] roads; // Картинки дорожек, которые буду рандомно выбирать
        PictureBox picBoxToMove; // ПикчерБокс персонажа, который я буду двигать - вообще неждан по названию =)
        PictureBox[] items;
        Greetings form; // Форма, которую я буду вызывать в случае ошибки/поздравления

        //Moving Moving.commandToAdd = new Moving(); // Информация о строке в коде, характеризующая движение - по мере распознавания строки заполняется, когда строка ПРАВИЛЬНО сформирована, добавляется в список команд, если неправильно - обнуляется !!! ТЕПЕРЬ ЭТО СТАТИЧЕСКОЕ ПОЛЕ В КЛАССЕ ДВИЖЕНИЕ!
        string[] terminals = { "даша", "башмачок", ".", "int method D", "int method S", "str method D", "str method S", "void method D", "void method S", "attribute D", "attribute S", "int attribute D", "int attribute S", "=", "number", "value", "если/пока", "(", ")", "ifEquals", "comparissons", ";", "string", "variable" }; // Терминалы нашего конечного автомата + обозначения того, что надо поискать в списках otherLexemes
        string[][] otherLexemes = {
                                      new string[] { "вверх(", "вниз(", "вправо(", "влево(" },  //int method Dora
                                      new string[] { "вверх(", "вниз(", "вправо(", "влево(" },  //int method Shoe
                                      new string[] {  },  //str method D
                                      new string[] { "прыжок(" },  //str method S
                                      new string[] { "взять(", "шаг_вверх(", "шаг_вниз(", "шаг_вправо(", "шаг_влево(" },  //void method D
                                      // 5
                                      new string[] { "шаг_вверх(", "шаг_вниз(", "шаг_вправо(", "шаг_влево(" },  //void method S
                                      new string[] { "неизвестный_предмет" },  //attribute D
                                      new string[] {  },  //attribute S
                                      new string[] { "вещей_в_рюкзаке" },  //int attribute D
                                      new string[] {  },   //int attribute S
                                      // 10
                                      new string[] { "==", "!=" },
                                      new string[] { ">", "<", ">=", "<=" },
                                      new string[] { "преграда", "дорога" },
                                      new string[] { "следующая_клетка" }, // Variables
                                      new string[] { "мухомор", "звезда" }
                                  };
        static int[][] DFA = {  /*   0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19  20  21  22  23  */
                        new int[] {  1, 12, -1,  3, -1,  5, -1,  4, -1,  9, -1,  7, -1, -1, -1, -1, 21, -1, -1, -1, -1, -1, -1, -1 },
                        new int[] { -1, -1,  2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
                        new int[] { -1, -1, -1,  3, -1,  5, -1,  4, -1,  9, -1,  7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 },
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  4, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  6, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  4, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  0, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  6, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  6, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, 13, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, 14, -1, 16, -1, 15, -1, 19, -1, 17, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 15, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  6, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 15, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 18, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  6, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 20, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  6, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 22, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { 23, 30, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 26 }, 
                        new int[] { -1, -1, 24, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, 26, -1, 25, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 27, 27, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 29, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 28, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1,  0, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 28, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, 31, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, 
                        new int[] { -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 26, -1, 25, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }
                      }; // А вот и он, гвоздь нашей программы)) страшный, правда?)
        #endregion
        public Form1(int location, int level)
        {
            InitializeComponent();
            dora.Size = new Size(d, d);
            shoe.Size = new Size(d, d);
            roads = new Image[] { Image.FromFile("loc" + location + "/roads/r1.bmp"), Image.FromFile("loc" + location + "/roads/r2.bmp"), Image.FromFile("loc" + location + "/roads/r3.bmp"), Image.FromFile("loc" + location + "/roads/r4.bmp"), Image.FromFile("loc" + location + "/roads/r5.bmp") };
            items = new PictureBox[] { item1, item2, item3 };
            dora.BackgroundImage = roads[0];
            shoe.BackgroundImage = roads[0];
            levelNow = level;
            locationNow = location;
            pictureBox1.BackColor = backColors[locationNow - 1];
            if (location < 4)
                codeBox.Height += pictureBox2.Height;
            ReadFile(location, level);
        }

        private void ReadFile(int location, int level)
        {
            // Считываю уровень из файла, если не нахожу такой файл - напоминаю себе, что такой уровень надо сделать
            try
            {
                using (StreamReader sr = new StreamReader(String.Format("loc" + location + "/leveltxts/" + level + ".txt"), Encoding.GetEncoding(1251)))
                {
                    for (int j = 0; j < p.Length; j++)
                    {
                        string[] str = sr.ReadLine().Split(' ');
                        p[j] = new int[str.Length];
                        for (int i = 0; i < str.Length; i++)
                        {
                            p[j][i] = Convert.ToInt32(str[i]);
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Сделать уровень " + location + "." + level + "!");
                Application.Exit();
            }
            Attributes.itemsInBackpack = 0;
            foreach (PictureBox pb in items)
                pb.Image = null;
            isMovingNow = false;
            b = true;
            pictureBox1_Paint(new object(), new PaintEventArgs(pictureBox1.CreateGraphics(), new Rectangle(new Point(0, 0), pictureBox1.Size)));
        }

        Random r = new Random();
        static int movingNum = 0; // Индекс списка необходимых перемещений, использую в таймере
        List<Moving> movingOrder = new List<Moving>(); // Список этих перемещений, заполняю по мере синтаксического разбора кода
        // Получаем номер строки нашего конечного автомата, с которого пытаемся пейти по ссылке на -1 строку и выдаем соответстующую ошибку
        private void catchError(int lineDFA)
        {
            int messageNumber = -1;
            switch (lineDFA)
            {
                case -1:
                    messageNumber = 1;
                    break;
                case 0:
                    messageNumber = 9;
                    break;
                case 1:
                    messageNumber = 10;
                    break;
                case 2:
                    messageNumber = 0;
                    break;
                case 3:
                    messageNumber = 3;
                    break;
                case 4:
                    messageNumber = 2;
                    break;
                //case 5:
                //    messageNumber = 8;
                //    break;
                case 6:
                    messageNumber = 5;
                    break;
                case 7:
                    messageNumber = 12;
                    break;
                case 8:
                    messageNumber = 3;
                    break;
                case 9:
                    messageNumber = 12;
                    break;
                case 10:
                    messageNumber = 12;
                    break;
                case 12:
                    messageNumber = 10;
                    break;
                case 13:
                    messageNumber = 11;
                    break;
                case 14:
                    messageNumber = 3;
                    break;
                case 15:
                    messageNumber = 2;
                    break;
                case 16:
                    messageNumber = 8;
                    break;
                case 17:
                    messageNumber = 12;
                    break;
                case 18:
                    messageNumber = 3;
                    break;
                case 19:
                    messageNumber = 12;
                    break;
                case 20:
                    messageNumber = 12;
                    break;
                case 21:
                    messageNumber = 2;
                    break;
                case 22:
                    messageNumber = 9;
                    break;
                case 23:
                    messageNumber = 10;
                    break;
                case 24:
                    messageNumber = 13;
                    break;
                case 25:
                    messageNumber = 14;
                    break;
                case 26:
                    messageNumber = 14;
                    break;
                case 27:
                    messageNumber = 3;
                    break;
                case 28:
                    messageNumber = 2;
                    break;
                case 29:
                    messageNumber = 12;
                    break;
                case 30:
                    messageNumber = 10;
                    break;
                case 31:
                    messageNumber = 16;
                    break;
            }
            Moving.commandToAdd = new Moving();
            try
            {
                form = new Greetings("error", messages[messageNumber]);
            }
            catch
            {
                form = new Greetings("error", "Неизвестная ошибка... =(");
            }
            form.ShowDialog();
            Moving.commandToAdd = new Moving();
            ReadFile(locationNow, levelNow);
            GO.Enabled = true;
            codeBox.ReadOnly = false;
        }
        private void walkingTimer_Tick(object sender, EventArgs e)
        {
            #region Если мы вышли за границу списка с движениями, приводим форму в нужное состояние
            if (movingNum >= movingOrder.Count)
            {
                dora.Visible = true;
                shoe.Visible = true;
                if (dora.Location == shoe.Location)
                {
                    if (strCount[locationNow - 1, levelNow - 1] >= codeBox.Lines.Length)
                        starCount = 3;
                    else if (strCount[locationNow - 1, levelNow - 1] + 1 == codeBox.Lines.Length)
                        starCount = 2;
                    else
                        starCount = 1;
                    if (levelNow == 5)
                    {
                        try
                        {
                            int maxAvailLoc = Convert.ToInt32(new StreamReader("locationInfo").ReadLine());
                            if (maxAvailLoc != locationNow)
                                new StreamWriter("locationInfo", false).Write(locationNow + 1);
                        }
                        catch (IOException)
                        {
                            walkingTimer.Enabled = false;
                            MessageBox.Show("Файл, необходимый для работы программы, был поврежден! К сожалению, весь прогресс был утерян.");
                            new StreamWriter("locationInfo", false).Write(0);
                        }
                    }
                    this.Hide();
                    form = new Greetings("congratulations", messages[4], locationNow, levelNow, starCount);
                }
                else
                {
                    form = new Greetings("error", messages[17]);
                    ReadFile(locationNow, levelNow);
                    b = true;
                }
                isMovingNow = false;
                b = true;
                walkingTimer.Enabled = false;
                form.ShowDialog();
                GO.Enabled = true;
                codeBox.ReadOnly = false;
                codeBox.Text = "";
                picBoxNow = picBoxToMove.Location;
                pictureBox1_Paint(new object(), new PaintEventArgs(pictureBox1.CreateGraphics(), new Rectangle(new Point(0, 0), pictureBox1.Size)));
            #endregion
            }
            try
            {
                if (movingOrder[movingNum].Condition.attribute1 == "unknownItem" &&
                    p[movingOrder[movingNum].CurrentCoords.Y][movingOrder[movingNum].CurrentCoords.X] > -5)
                {
                    walkingTimer.Enabled = false;
                    new Greetings("error", messages[21]).ShowDialog();
                    ReadFile(locationNow, levelNow);
                    codeBox.Text = "";
                    Moving.commandToAdd = new Moving();
                    GO.Enabled = true;
                    codeBox.ReadOnly = false;
                    return;
                }
                if (movingOrder[movingNum].Info != "take")
                {
                    int speed = 2;
                    picBoxToMove = movingOrder[movingNum].CharacterToMove == "dora" ? dora : shoe;
                    #region Если нужный ПикчерБокс пока не в тех координатах, где он должен остановится, то вычисляем направление и добавляем его, если Даша и Башмак совпадают, поздравляем
                    if (picBoxToMove.Location != new Point(d * movingOrder[movingNum].CoordsAfterStep.X + fieldLocation.X, d * movingOrder[movingNum].CoordsAfterStep.Y + fieldLocation.Y))
                    {
                        int signX = Convert.ToInt32(movingOrder[movingNum].CoordsAfterStep.X > movingOrder[movingNum].CurrentCoords.X) - Convert.ToInt32(movingOrder[movingNum].CoordsAfterStep.X < movingOrder[movingNum].CurrentCoords.X);
                        int signY = Convert.ToInt32(movingOrder[movingNum].CoordsAfterStep.Y > movingOrder[movingNum].CurrentCoords.Y) - Convert.ToInt32(movingOrder[movingNum].CoordsAfterStep.Y < movingOrder[movingNum].CurrentCoords.Y);

                        movingOrder[movingNum].ObjectToMove.Top += (signY * speed);
                        movingOrder[movingNum].ObjectToMove.Left += (signX * speed);
                    }
                    //Иначе переходим к следующему движению в списке
                    else
                    {
                        movingNum++;
                        picBoxToMove = movingOrder[movingNum].ObjectToMove;
                        picBoxNow = new Point(picBoxToMove.Left, picBoxToMove.Top);
                    }
                    #endregion
                }
                else if (movingOrder[movingNum].Condition.isAvailible(movingOrder.ToArray(), movingNum))
                {
                    if (p[movingOrder[movingNum].CurrentCoords.Y][movingOrder[movingNum].CurrentCoords.X] == -3 ||
                        p[movingOrder[movingNum].CurrentCoords.Y][movingOrder[movingNum].CurrentCoords.X] == -5)
                    {
                        p[movingOrder[movingNum].CurrentCoords.Y][movingOrder[movingNum].CurrentCoords.X] = 0;
                        Attributes.itemsInBackpack++;
                        items[Attributes.itemsInBackpack - 1].Image = Image.FromFile("bgimages/star.png");
                        movingNum++;
                    }
                    else
                    {
                        walkingTimer.Enabled = false;
                        if (p[movingOrder[movingNum].CurrentCoords.Y][movingOrder[movingNum].CurrentCoords.X] == -4 ||
                            p[movingOrder[movingNum].CurrentCoords.Y][movingOrder[movingNum].CurrentCoords.X] == -6)
                        {
                            Attributes.itemsInBackpack++;
                            items[Attributes.itemsInBackpack - 1].Image = Image.FromFile("bgimages/mushroom.png");
                            new Greetings("error", messages[20]).ShowDialog();
                        }
                        else
                            new Greetings("error", messages[18]).ShowDialog();
                        ReadFile(locationNow, levelNow);
                        codeBox.Text = "";
                        Moving.commandToAdd = new Moving();
                        GO.Enabled = true;
                        codeBox.ReadOnly = false;
                    }
                }
                else
                    movingNum++;
            }
            catch (ArgumentOutOfRangeException)
            { }

        }
        // Для вычисления текущих координат ПикчерБокса - если раньше для него уже было записано движение, то координаты стали не те, что мы храним в переменной
        private int FindPrevious()
        {
            for (int i = movingOrder.Count - 1; i >= 0; i--)
            {
                if (movingOrder[i].CharacterToMove == charToMove && movingOrder[i].Condition.isAvailible(movingOrder.ToArray(), i))
                {
                    return i;
                }
            }
            return -1;
        }
        // !!!!! Это важно, тут я ищу соответствие по текущему терминалу и, если нахожу, то запоминаю определенную информацию в Moving.commandToAdd
        private int FindTerminal(string lexeme, int currentDFA)
        {
            for (int i = 0; i < terminals.Length; i++)
            {
                switch (i)
                {
                    case 0:
                        if (terminals[i] == lexeme) // Если написано "даша"
                        {
                            charToMove = "dora";
                            int prev = FindPrevious();
                            Moving.commandToAdd.CharacterToMove = "dora";
                            Moving.commandToAdd.ObjectToMove = dora;
                            Moving.commandToAdd.CurrentCoords = prev == -1 ? new Point((dora.Left - fieldLocation.X) / d, (dora.Top - fieldLocation.Y) / d) : movingOrder[prev].CoordsAfterStep;
                            return i;
                        }
                        else if (lexeme == "если")
                        {
                            return 16;
                        }
                        else if (lexeme == "пока")
                        {
                            Moving.commandToAdd.WHILE = true;
                            return 16;
                        }
                        break;
                    case 1:
                        if (terminals[i] == lexeme) // Если написано "башмачок"
                        {
                            charToMove = "shoe";
                            int prev = FindPrevious();
                            Moving.commandToAdd.CharacterToMove = "shoe";
                            Moving.commandToAdd.ObjectToMove = shoe;
                            Moving.commandToAdd.CurrentCoords = prev == -1 ? new Point((shoe.Left - fieldLocation.X) / d, (shoe.Top - fieldLocation.Y) / d) : movingOrder[prev].CoordsAfterStep;
                            return i;
                        }
                        break;
                    case 3:
                        for (int j = 0; j < otherLexemes[0].Length && (Moving.commandToAdd.CharacterToMove == "dora" || Moving.commandToAdd.CharacterToMove == null); j++) // Инт методы Даши
                        {
                            switch (j)
                            {
                                case 0:
                                    if (otherLexemes[0][j] == lexeme)
                                    {
                                        Moving.commandToAdd.Direction = new Point(0, -1);
                                        return 3;
                                    }
                                    break;
                                case 1:
                                    if (otherLexemes[0][j] == lexeme)
                                    {
                                        Moving.commandToAdd.Direction = new Point(0, 1);
                                        return 3;
                                    }
                                    break;
                                case 2:
                                    if (otherLexemes[0][j] == lexeme)
                                    {
                                        Moving.commandToAdd.Direction = new Point(1, 0);
                                        return 3;
                                    }
                                    break;
                                case 3:
                                    if (otherLexemes[0][j] == lexeme)
                                    {
                                        Moving.commandToAdd.Direction = new Point(-1, 0);
                                        return 3;
                                    }
                                    break;
                            }
                        }
                        break;
                    case 4:
                        for (int j = 0; j < otherLexemes[1].Length && Moving.commandToAdd.CharacterToMove == "shoe"; j++) // Инт методы Башмачка
                        {
                            switch (j)
                            {
                                case 0:
                                    if (otherLexemes[1][j] == lexeme)
                                    {
                                        Moving.commandToAdd.Direction = new Point(0, -1);
                                        return 4;
                                    }
                                    break;
                                case 1:
                                    if (otherLexemes[1][j] == lexeme)
                                    {
                                        Moving.commandToAdd.Direction = new Point(0, 1);
                                        return 4;
                                    }
                                    break;
                                case 2:
                                    if (otherLexemes[1][j] == lexeme)
                                    {
                                        Moving.commandToAdd.Direction = new Point(1, 0);
                                        return 4;
                                    }
                                    break;
                                case 3:
                                    if (otherLexemes[1][j] == lexeme)
                                    {
                                        Moving.commandToAdd.Direction = new Point(-1, 0);
                                        return 4;
                                    }
                                    break;
                            }
                        }
                        break;
                    case 5: // Не дописано! Стринг методы Даши
                        for (int j = 0; j < otherLexemes[2].Length && (Moving.commandToAdd.CharacterToMove == "dora" || Moving.commandToAdd.CharacterToMove == null); j++)
                        {
                            switch (j)
                            {
                                default:
                                    if (lexeme == otherLexemes[2][j])
                                        return 5;
                                    break;
                            }
                        }
                        break;
                    case 6: // Не дописано! Стринг методы Башмачка
                        for (int j = 0; j < otherLexemes[3].Length && Moving.commandToAdd.CharacterToMove == "shoe"; j++)
                        {
                            switch (j)
                            {
                                case 0:
                                    if (lexeme == otherLexemes[3][j])
                                    {
                                        Moving.commandToAdd.Info = "jump";
                                        return 6;
                                    }
                                    break;
                            }
                        }
                        break;
                    case 7: // Не дописано! Воид методы Даши
                        for (int j = 0; j < otherLexemes[4].Length && (Moving.commandToAdd.CharacterToMove == "dora" || Moving.commandToAdd.CharacterToMove == null); j++)
                        {
                            switch (j)
                            {
                                case 0:
                                    if (lexeme == otherLexemes[4][j])
                                    {
                                        Moving.commandToAdd.Info = "take";
                                        Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X + Moving.commandToAdd.Direction.X, Moving.commandToAdd.CurrentCoords.Y + Moving.commandToAdd.Direction.Y);
                                        Attributes.itemsInBackpack++;
                                        return i;
                                    }
                                    break;
                                case 1:
                                    if (lexeme == otherLexemes[4][j])
                                    {
                                        Moving.commandToAdd.Direction = new Point(0, -1);
                                        Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X + Moving.commandToAdd.Direction.X, Moving.commandToAdd.CurrentCoords.Y + Moving.commandToAdd.Direction.Y);
                                        return i;
                                    }
                                    break;
                                case 2:
                                    if (lexeme == otherLexemes[4][j])
                                    {
                                        Moving.commandToAdd.Direction = new Point(0, 1);
                                        Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X + Moving.commandToAdd.Direction.X, Moving.commandToAdd.CurrentCoords.Y + Moving.commandToAdd.Direction.Y);
                                        return i;
                                    }
                                    break;
                                case 3:
                                    if (lexeme == otherLexemes[4][j])
                                    {
                                        Moving.commandToAdd.Direction = new Point(1, 0);
                                        Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X + Moving.commandToAdd.Direction.X, Moving.commandToAdd.CurrentCoords.Y + Moving.commandToAdd.Direction.Y);
                                        return i;
                                    }
                                    break;
                                case 4:
                                    if (lexeme == otherLexemes[4][j])
                                    {
                                        Moving.commandToAdd.Direction = new Point(-1, 0);
                                        Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X + Moving.commandToAdd.Direction.X, Moving.commandToAdd.CurrentCoords.Y + Moving.commandToAdd.Direction.Y);
                                        return i;
                                    }
                                    break;
                            }
                        }
                        break;
                    case 8: // Воид методы Башмачка
                        for (int j = 0; j < otherLexemes[5].Length && Moving.commandToAdd.CharacterToMove == "shoe"; j++)
                        {
                            switch (j)
                            {
                                case 0:
                                    if (lexeme == otherLexemes[5][j])
                                    {
                                        Moving.commandToAdd.Direction = new Point(0, -1);
                                        Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X + Moving.commandToAdd.Direction.X, Moving.commandToAdd.CurrentCoords.Y + Moving.commandToAdd.Direction.Y);
                                        return i;
                                    }
                                    break;
                                case 1:
                                    if (lexeme == otherLexemes[5][j])
                                    {
                                        Moving.commandToAdd.Direction = new Point(0, 1);
                                        Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X + Moving.commandToAdd.Direction.X, Moving.commandToAdd.CurrentCoords.Y + Moving.commandToAdd.Direction.Y);
                                        return i;
                                    }
                                    break;
                                case 2:
                                    if (lexeme == otherLexemes[5][j])
                                    {
                                        Moving.commandToAdd.Direction = new Point(1, 0);
                                        Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X + Moving.commandToAdd.Direction.X, Moving.commandToAdd.CurrentCoords.Y + Moving.commandToAdd.Direction.Y);
                                        return i;
                                    }
                                    break;
                                case 3:
                                    if (lexeme == otherLexemes[5][j])
                                    {
                                        Moving.commandToAdd.Direction = new Point(-1, 0);
                                        Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X + Moving.commandToAdd.Direction.X, Moving.commandToAdd.CurrentCoords.Y + Moving.commandToAdd.Direction.Y);
                                        return i;
                                    }
                                    break;
                            }
                        }
                        break;
                    case 9: // Не дописано! Свойства Даши
                        for (int j = 0; j < otherLexemes[6].Length; j++)
                        {
                            switch (j)
                            {
                                case 0:
                                    if (lexeme == otherLexemes[6][j])
                                    {
                                        Moving.commandToAdd.Condition.attribute1 = "unknownItem";
                                        return i;
                                    }
                                    break;
                            }
                        }
                        break;
                    case 10: // Не дописано! Свойства Башмачка
                        for (int j = 0; j < otherLexemes[7].Length; j++)
                        {
                            switch (j)
                            {
                                default:
                                    if (lexeme == otherLexemes[7][j])
                                        return i;
                                    break;
                            }
                        }
                        break;
                    case 11: // Не дописано! Инт свойства Даши
                        for (int j = 0; j < otherLexemes[8].Length; j++)
                        {
                            switch (j)
                            {
                                case 0:
                                    if (lexeme == otherLexemes[8][j])
                                    {
                                        Moving.commandToAdd.Condition.attribute1 = "itemsInBackpack";
                                        return i;
                                    }
                                    break;
                            }
                        }
                        break;
                    case 12: // Не дописано! Инт свойства Башмачка
                        foreach (string lex in otherLexemes[9])
                        {
                            if (lex == lexeme)
                            {
                                return i;
                            }
                        }
                        break;
                    case 14: // Число
                        try
                        {
                            int q = Convert.ToInt32(lexeme);
                            if (currentDFA == 3 || currentDFA == 14)
                                Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X + Moving.commandToAdd.Direction.X * q, Moving.commandToAdd.CurrentCoords.Y + Moving.commandToAdd.Direction.Y * q);
                            else if (currentDFA == 27)
                            {
                                Moving.commandToAdd.Condition.cond2 = q;
                            }
                            return i;
                        }
                        catch { }
                        break;
                    case 15:        // Значение для некоторого свойства
                        if (Moving.commandToAdd.Condition.attribute1 == "nextCell")
                            for (int j = 0; j < otherLexemes[12].Length; j++)
                            {
                                switch (j)
                                {
                                    case 0:
                                        if (otherLexemes[12][j] == lexeme)
                                        {
                                            Moving.commandToAdd.Condition.cond2 = 1;
                                            return i;
                                        }
                                        break;
                                    case 1:
                                        if (otherLexemes[12][j] == lexeme)
                                        {
                                            Moving.commandToAdd.Condition.cond2 = 0;
                                            return i;
                                        }
                                        break;
                                    default:
                                        catchError(currentDFA);
                                        return -1;
                                }
                            }
                        else if (Moving.commandToAdd.Condition.attribute1 == "unknownItem")
                            for (int j = 0; j < otherLexemes[14].Length; j++)
                            {
                                switch (j)
                                {
                                    case 0:
                                        if (otherLexemes[14][j] == lexeme)
                                        {
                                            Moving.commandToAdd.Condition.cond2 = 1;
                                            return i;
                                        }
                                        break;
                                    case 1:
                                        if (otherLexemes[14][j] == lexeme)
                                        {
                                            Moving.commandToAdd.Condition.cond2 = 0;
                                            return i;
                                        }
                                        break;
                                    default:
                                        catchError(currentDFA);
                                        return -1;
                                }
                            }
                        break;
                    case 16:        // Если/пока // Не дописано!

                        break;
                    case 19: // Сравнители
                        for (int j = 0; j < otherLexemes[11].Length; j++)
                        {
                            if (otherLexemes[11][j] == lexeme)
                            {
                                Moving.commandToAdd.Condition.comparisson = otherLexemes[11][j];
                                return i;
                            }
                        }
                        break;
                    case 20: // ifEquals
                        for (int j = 0; j < otherLexemes[10].Length; j++)
                        {
                            if (otherLexemes[10][j] == lexeme)
                            {
                                Moving.commandToAdd.Condition.comparisson = otherLexemes[10][j];
                                return (currentDFA == 25) ? 20 : 19;
                            }
                        }
                        break;
                    case 21:
                        if (lexeme == ";")
                        {
                            ifEndOfCode = true;
                            movingOrder.Add(Moving.commandToAdd);
                            Moving.commandToAdd = new Moving();
                            return i;
                        }
                        break;
                    case 22:
                        if (Moving.commandToAdd.Info == "jump")
                        {
                            switch (lexeme)
                            {
                                case "вверх":
                                    Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X, Moving.commandToAdd.CurrentCoords.Y - 2);
                                    return i;
                                case "вниз":
                                    Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X, Moving.commandToAdd.CurrentCoords.Y + 2);
                                    return i;
                                case "вправо":
                                    Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X + 2, Moving.commandToAdd.CurrentCoords.Y);
                                    return i;
                                case "влево":
                                    Moving.commandToAdd.CoordsAfterStep = new Point(Moving.commandToAdd.CurrentCoords.X - 2, Moving.commandToAdd.CurrentCoords.Y);
                                    return i;
                            }
                        }
                        break;
                    case 23:
                        for (int j = 0; j < otherLexemes[13].Length; j++)
                        {
                            switch (j)
                            {
                                case 0:
                                    if (otherLexemes[13][j] == lexeme)
                                    {
                                        Moving.commandToAdd.Condition.attribute1 = "nextCell";
                                        return i;
                                    }
                                    break;
                            }
                        }
                        break;
                    default:
                        if (terminals[i] == lexeme)
                        {
                            return i;
                        }
                        break;
                }
            }
            return -1;
        }
        // Осуществляю переход по строкам конечного автомата, учитывая текущий терминал, если мы его не нашли, получаем ИндексАутОфРенджЕксепшн и выбрасываем ошибку
        private bool updateCurDFA(string terminal, ref int curDFA)
        {
            if (terminal == "")
                return true;
            int oldDFA = curDFA;
            try
            {
                curDFA = DFA[curDFA][FindTerminal(terminal, curDFA)];
            }
            catch
            {
                catchError(oldDFA);
                return false;
            }
            if (curDFA < 0)
            {
                catchError(oldDFA);
                return false;
            }
            return true;
        }

        // А вот оно, то, из-за чего никаких ошибок не выдавалось - без этого мы не умеем воспринимать конец строки
        bool ifEndOfCode = false;
        private bool SyntaxAnalysis(string code)
        {
            code = Regex.Replace(codeBox.Text, " \\(", "(");
            string lex = ""; // Будущая лексема
            int currentDFALine = 0; // Строка конечного автомата
            for (int q = 0; q < code.Length; q++)
            {
                if (Regex.IsMatch(code[q].ToString(), "\\w|-|_")) // Если текущий символ - буквы или цифра (или "-" для отрицательных чисел)
                {
                    ifEndOfCode = false;
                    if (lex.Length != 0)
                        if (Regex.IsMatch(lex[lex.Length - 1].ToString(), ">|<|=|=|!"))
                        {
                            if (!updateCurDFA(lex.ToLower(), ref currentDFALine))
                                return false;
                            lex = "";
                        }
                    lex += code[q];
                }
                else if (Regex.IsMatch(code[q].ToString(), ">|<|=|=|!")) // Если текущий символ - буквы или цифра (или "-" для отрицательных чисел)
                {
                    ifEndOfCode = false;
                    if (lex.Length != 0)
                        if (Regex.IsMatch(lex[lex.Length - 1].ToString(), "\\w|-|_"))
                        {
                            if (!updateCurDFA(lex.ToLower(), ref currentDFALine))
                                return false;
                            lex = "";
                        }
                    lex += code[q];
                }
                else if (code[q] == '\r' || code[q] == '\n') // Если это энтер
                { }
                else if (code[q] == ' ')
                {
                    ifEndOfCode = false;
                    if (!updateCurDFA(lex.ToLower(), ref currentDFALine))
                        return false;
                    lex = "";
                }
                else
                {
                    if (currentDFALine == 6) // Если у нас считана ";"
                    { }
                    else if (currentDFALine == 2 || currentDFALine == 13 || (currentDFALine == 0 && lex.ToLower() != "даша" && lex.ToLower() != "башмачок" && lex.ToLower() != "если" && lex.ToLower() != "пока")) // Для случаев если/пока и методов/свойств
                    {
                        ifEndOfCode = false;
                        if (currentDFALine == 0 && lex.ToLower() != "даша" && lex.ToLower() != "башмачок") // Обращение к Даше без указания (просто "вверх(3);")
                        {
                            charToMove = "dora";
                            int prev = FindPrevious();
                            Moving.commandToAdd.CharacterToMove = "dora";
                            Moving.commandToAdd.ObjectToMove = dora;
                            Moving.commandToAdd.CurrentCoords = prev == -1 ? new Point((dora.Left - fieldLocation.X) / d, (dora.Top - fieldLocation.Y) / d) : movingOrder[prev].CoordsAfterStep;
                        }
                        if (code[q] != '(') // Если после считывания метода или если-пока не скобка - выкидываем ошибку
                        {
                            catchError(4);
                            return false;
                        }
                        lex += code[q]; // Начисляем к терминалу скобку
                        if (!updateCurDFA(lex.ToLower(), ref currentDFALine))
                            return false;
                        lex = ""; // Обнуляем
                        continue;
                    }
                    else if (lex.ToLower() == "если") // если
                    {
                        ifEndOfCode = false;
                        if (!updateCurDFA(lex.ToLower(), ref currentDFALine))
                            return false;
                    }
                    else if (lex.ToLower() == "пока") // пока
                    {
                        ifEndOfCode = false;
                        if (!updateCurDFA(lex.ToLower(), ref currentDFALine))
                            return false;
                    }
                    else // Для других случаев вроде закрывающейся скобки переходим по считанному в lex терминалу, обнуляем его, переходим по только что считанному символу
                    {
                        ifEndOfCode = false;
                        if (!updateCurDFA(lex.ToLower(), ref currentDFALine))
                            return false;
                    }
                    lex = "";
                    if (!updateCurDFA(code[q].ToString(), ref currentDFALine))
                        return false;
                }
            }
            if (ifEndOfCode) // Если мы получили конец строки (в ФайндТерминал в случае ";" я его изменяю на тру)
                return true;
            else
            {
                catchError(6);
                return false;
            }
        }
        private string CodeFormat(string code)
        {
            string res = "";
            for (int q = 0; q < code.Length; q++)
            {
                switch (code[q])
                {
                    case ' ':
                        continue;
                    case '\r':
                        continue;
                    case '\n':
                        continue;
                    case ';':
                        res += code[q] + "\r\n";
                        //q++;
                        break;
                    case '(':
                        if (res[res.Length - 1] == 'и' || res[res.Length - 1] == 'а')
                            res += ' ';
                        res += code[q];
                        break;
                    case ')':
                        res += code[q];
                        if (code[q + 1] != ';')
                        {
                            res += ' ';
                            q++;
                        }
                        break;
                    default:
                        res += code[q];
                        break;
                }
            }
            return res.TrimEnd('\r', '\n');
        }
        private void GO_Click(object sender, EventArgs e)
        {
            codeBox.ReadOnly = true;
            GO.Enabled = false;
            //codeBox.Text = Regex.Replace(codeBox.Text, "\r\n", "");
            //codeBox.Text = Regex.Replace(codeBox.Text, ";", ";\r\n");
            //codeBox.Text = Regex.Replace(codeBox.Text, "  ", " ");
            codeBox.Text = CodeFormat(codeBox.Text);
            movingOrder.Clear();
            movingNum = 0;
            try
            {
                if (!SyntaxAnalysis(codeBox.Text)) // SyntaxAnalysis возвращает фолс, если нашел ошибку
                {
                    codeBox.Enabled = true;
                    GO.Enabled = true;
                    return;
                }
                for (int move = 0; move < movingOrder.Count; move++) // Проверяем наличие препятствий для каждого персонажа по списку передвижений
                {                    
                    if (movingOrder[move].Condition.isAvailible(movingOrder.ToArray(), move))
                    {
                        if (movingOrder[move].WHILE)
                        {
                            if (movingOrder[move].CoordsAfterStep == movingOrder[move].CurrentCoords)
                            {
                                new Greetings("error", messages[19]).ShowDialog();
                                return;
                            }
                            movingOrder.Insert(move + 1, new Moving(movingOrder[move]));
                            Point step = new Point(movingOrder[move + 1].CoordsAfterStep.X - movingOrder[move + 1].CurrentCoords.X, movingOrder[move + 1].CoordsAfterStep.Y - movingOrder[move + 1].CurrentCoords.Y);
                            movingOrder[move + 1].CurrentCoords = movingOrder[move].CoordsAfterStep;
                            movingOrder[move + 1].CoordsAfterStep = new Point(movingOrder[move + 1].CoordsAfterStep.X + step.X, movingOrder[move + 1].CoordsAfterStep.Y + step.Y);
                        }
                    }
                    if (movingOrder[move].Info != "take")
                    {
                        if (movingOrder[move].Condition.isAvailible(movingOrder.ToArray(), move))
                        {
                            int signX = Convert.ToInt32(movingOrder[move].CoordsAfterStep.X > movingOrder[move].CurrentCoords.X) - Convert.ToInt32(movingOrder[move].CoordsAfterStep.X < movingOrder[move].CurrentCoords.X);
                            int signY = Convert.ToInt32(movingOrder[move].CoordsAfterStep.Y > movingOrder[move].CurrentCoords.Y) - Convert.ToInt32(movingOrder[move].CoordsAfterStep.Y < movingOrder[move].CurrentCoords.Y);
                            if (movingOrder[move].Info != "jump")
                            {
                                for (int x = movingOrder[move].CurrentCoords.X + signX, y = movingOrder[move].CurrentCoords.Y + signY; movingOrder[move].CoordsAfterStep != new Point(x - signX, y - signY); x += signX, y += signY)
                                    if (p[y][x] > 0)
                                    {
                                        catchError(-1);
                                        return;
                                    }
                            }
                            else
                            {
                                if (p[movingOrder[move].CoordsAfterStep.Y][movingOrder[move].CoordsAfterStep.X] > 0)
                                {
                                    new Greetings("error", messages[7]).ShowDialog();
                                    ReadFile(locationNow, levelNow);
                                    Moving.commandToAdd = new Moving();
                                    GO.Enabled = true;
                                    codeBox.ReadOnly = false;
                                    codeBox.Text = "";
                                    return;
                                }
                            }
                        }
                        else
                        {
                            MissStep(move--);
                        }
                    }
                }
                picBoxToMove = movingOrder[movingNum].ObjectToMove;
                picBoxNow = new Point(picBoxToMove.Location.X, picBoxToMove.Location.Y);
                movingNum = 0;
                isMovingNow = true;
                Attributes.itemsInBackpack = 0;
                walkingTimer.Enabled = true;
            }
            catch (IndexOutOfRangeException) // Исключение, которое вообще-то выбраcываться уже не должно, выдает "Неизвестная ошибка"
            {
                catchError(-1);
            }
        }

        private void MissStep(int move)
        {
            movingOrder.RemoveAt(move--);
            for (int i = move; i < movingOrder.Count - 1; i++)
            {
                Point nextStep = new Point(movingOrder[move + 1].CoordsAfterStep.X - movingOrder[move + 1].CurrentCoords.X, movingOrder[move + 1].CoordsAfterStep.Y - movingOrder[move + 1].CurrentCoords.Y);
                movingOrder[move + 1].CurrentCoords = movingOrder[move].CoordsAfterStep;
                movingOrder[move + 1].CoordsAfterStep = new Point(movingOrder[move + 1].CurrentCoords.X + nextStep.X, movingOrder[move + 1].CurrentCoords.Y + nextStep.Y);
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new Hint(locationNow, levelNow).ShowDialog();
        }
        int paint = 0;
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {
            paint++;
            int unknownStarAmount = 0;
            int unknowAmount = 0;
            for (int i = 0; i < p.Length && !isMovingNow; i++)
            {
                for (int j = 0; j < p[i].Length; j++)
                {
                    switch (p[i][j])
                    {
                        case 0:
                            e.Graphics.DrawImage(roads[r.Next(roads.Length)], j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            break;
                        case 1:
                            e.Graphics.DrawImage(Image.FromFile("loc" + locationNow + "/roads/o.png"), j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            break;
                        case 2:
                            if (p[i][j + 1] == 1)
                                e.Graphics.DrawImage(Image.FromFile("loc" + locationNow + "/roads/o.png"), j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            else
                                e.Graphics.DrawImage(roads[r.Next(roads.Length)], j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            if (p[i - ((i == 0) ? -1 : 1)][j] == 2 || p[i + ((i == 0) ? -1 : 1)][j] == 2)
                                e.Graphics.DrawImage(Image.FromFile(/*"loc" + locationNow + "/*/"obstacles/vert.png"), j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            else if (p[i][j - 1] == 2 || p[i][j + 1] == 2)
                                e.Graphics.DrawImage(Image.FromFile(/*"loc" + locationNow + "/*/"obstacles/hor.png"), j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            else
                                e.Graphics.DrawImage(Image.FromFile(/*"loc" + locationNow + "/*/"obstacles/hor.png"), j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            break;
                        case -1:
                            e.Graphics.DrawImage(roads[r.Next(roads.Length)], j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            if (b)
                            {
                                dora.Location = new Point(fieldLocation.X + j * d, fieldLocation.Y + i * d);
                                dora.Visible = true;
                                picBoxToMove = dora;
                            }
                            break;
                        case -2:
                            e.Graphics.DrawImage(roads[r.Next(roads.Length)], j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            if (b)
                            {
                                shoe.Location = new Point(fieldLocation.X + j * d, fieldLocation.Y + i * d);
                                shoe.Visible = true;
                            }
                            break;
                        case -3:
                            e.Graphics.DrawImage(roads[r.Next(roads.Length)], j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            e.Graphics.DrawImage(Image.FromFile("bgimages/star.png"), j * d + fieldLocation.X + d / 5, i * d + fieldLocation.Y + d / 5, 3 * d / 5, 3 * d / 5);
                            break;
                        case -4:
                            e.Graphics.DrawImage(roads[r.Next(roads.Length)], j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            e.Graphics.DrawImage(Image.FromFile("bgimages/mushroom.png"), j * d + fieldLocation.X + d / 5, i * d + fieldLocation.Y + d / 5, 3 * d / 5, 3 * d / 5);
                            break;
                        case -5:
                            e.Graphics.DrawImage(roads[r.Next(roads.Length)], j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            e.Graphics.DrawImage(Image.FromFile("bgimages/unknown.png"), j * d + fieldLocation.X + d / 5, i * d + fieldLocation.Y + d / 5, 3 * d / 5, 3 * d / 5);
                            break;
                        case -6:
                            e.Graphics.DrawImage(roads[r.Next(roads.Length)], j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            e.Graphics.DrawImage(Image.FromFile("bgimages/unknown.png"), j * d + fieldLocation.X + d / 5, i * d + fieldLocation.Y + d / 5, 3 * d / 5, 3 * d / 5);
                            break;
                        case -7:
                            e.Graphics.DrawImage(roads[r.Next(roads.Length)], j * d + fieldLocation.X, i * d + fieldLocation.Y, d, d);
                            e.Graphics.DrawImage(Image.FromFile("bgimages/unknown.png"), j * d + fieldLocation.X + d / 5, i * d + fieldLocation.Y + d / 5, 3 * d / 5, 3 * d / 5);
                            if (5 - unknowAmount == 3 - unknownStarAmount)
                            {
                                p[i][j] += 2;
                                unknownStarAmount++;
                            }
                            else if (unknownStarAmount == 3)
                            {
                                p[i][j]++;
                            }
                            else
                            {
                                p[i][j] += r.Next(1, 3);
                                if (p[i][j] == -5) unknownStarAmount++;
                            }
                            unknowAmount++;
                            break;
                    }
                }
            }
            if (isMovingNow)
            {
                try
                {
                    int signX = Convert.ToInt32(movingOrder[movingNum].CoordsAfterStep.X > movingOrder[movingNum].CurrentCoords.X) - Convert.ToInt32(movingOrder[movingNum].CoordsAfterStep.X < movingOrder[movingNum].CurrentCoords.X);
                    int signY = Convert.ToInt32(movingOrder[movingNum].CoordsAfterStep.Y > movingOrder[movingNum].CurrentCoords.Y) - Convert.ToInt32(movingOrder[movingNum].CoordsAfterStep.Y < movingOrder[movingNum].CurrentCoords.Y);
                    for (Point step = movingOrder[movingNum].CurrentCoords; step != movingOrder[movingNum].CoordsAfterStep; step.X += signX, step.Y += signY)
                    {
                        switch (p[step.Y][step.X])
                        {
                            case 0: // Дорога
                                e.Graphics.DrawImage(roads[r.Next(roads.Length)], step.X * d + fieldLocation.X, step.Y * d + fieldLocation.Y, d, d);
                                break;
                            case 1: // Преграда
                                e.Graphics.DrawImage(Image.FromFile("loc" + locationNow + "/roads/o.png"), step.X * d + fieldLocation.X, step.Y * d + fieldLocation.Y, d, d);
                                break;
                            case 2: // Забор
                                if (p[step.Y][step.X + 1] == 1)
                                    e.Graphics.DrawImage(Image.FromFile("loc" + locationNow + "/roads/o.png"), step.X * d + fieldLocation.X, step.Y * d + fieldLocation.Y, d, d);
                                else
                                    e.Graphics.DrawImage(roads[r.Next(roads.Length)], step.X * d + fieldLocation.X, step.Y * d + fieldLocation.Y, d, d);
                                if (p[step.Y - ((step.Y == 0) ? -1 : 1)][step.X] == 2 || p[step.Y + ((step.Y == 0) ? -1 : 1)][step.X] == 2)
                                    e.Graphics.DrawImage(Image.FromFile(/*"loc" + locationNow + "/*/"obstacles/vert.png"), step.X * d + fieldLocation.X, step.Y * d + fieldLocation.Y, d, d);
                                else if (p[step.Y][step.X - 1] == 2 || p[step.Y][step.X + 1] == 2)
                                    e.Graphics.DrawImage(Image.FromFile(/*"loc" + locationNow + "/*/"obstacles/hor.png"), step.X * d + fieldLocation.X, step.Y * d + fieldLocation.Y, d, d);
                                else
                                    e.Graphics.DrawImage(Image.FromFile(/*"loc" + locationNow + "/*/"obstacles/hor.png"), step.X * d + fieldLocation.X, step.Y * d + fieldLocation.Y, d, d);
                                break;
                            case -3: // Звезда
                                e.Graphics.DrawImage(roads[r.Next(roads.Length)], step.X * d + fieldLocation.X, step.Y * d + fieldLocation.Y, d, d);
                                e.Graphics.DrawImage(Image.FromFile("bgimages/star.png"), step.X * d + fieldLocation.X + d / 5, step.Y * d + fieldLocation.Y + d / 5, 3 * d / 5, 3 * d / 5);
                                break;
                            case -4: // Мухоморчик
                                e.Graphics.DrawImage(roads[r.Next(roads.Length)], step.X * d + fieldLocation.X, step.Y * d + fieldLocation.Y, d, d);
                                e.Graphics.DrawImage(Image.FromFile("bgimages/mushroom.png"), step.X * d + fieldLocation.X + d / 5, step.Y * d + fieldLocation.Y + d / 5, 3 * d / 5, 3 * d / 5);
                                break;
                            case -5: // Звезда - неизв.
                                e.Graphics.DrawImage(roads[r.Next(roads.Length)], step.X * d + fieldLocation.X, step.Y * d + fieldLocation.Y, d, d);
                                e.Graphics.DrawImage(Image.FromFile("bgimages/unknown.png"), step.X * d + fieldLocation.X + d / 5, step.Y * d + fieldLocation.Y + d / 5, 3 * d / 5, 3 * d / 5);
                                break;
                            case -6: // Гриб - неизв.
                                e.Graphics.DrawImage(roads[r.Next(roads.Length)], step.X * d + fieldLocation.X, step.Y * d + fieldLocation.Y, d, d);
                                e.Graphics.DrawImage(Image.FromFile("bgimages/unknown.png"), step.X * d + fieldLocation.X + d / 5, step.Y * d + fieldLocation.Y + d / 5, 3 * d / 5, 3 * d / 5);
                                break;
                            case -7: // Неизвестный предмет
                                e.Graphics.DrawImage(roads[r.Next(roads.Length)], step.X * d + fieldLocation.X, step.Y * d + fieldLocation.Y, d, d);
                                e.Graphics.DrawImage(Image.FromFile("bgimages/unknown.png"), step.X * d + fieldLocation.X + d / 5, step.Y * d + fieldLocation.Y + d / 5, 3 * d / 5, 3 * d / 5);
                                p[step.Y][step.X] += r.Next(1, 3);
                                break;
                        }
                    }
                }
                catch { }
            }
            for (int i = 1; i < p[0].Length; i++)
            {
                e.Graphics.DrawLine(new Pen(Color.Silver), fieldLocation.X + d * i, fieldLocation.Y + d, fieldLocation.X + d * i, fieldLocation.Y + 16 * d);
            }
            for (int i = 1; i < p.Length; i++)
            {
                e.Graphics.DrawLine(new Pen(Color.Silver), fieldLocation.X + d, fieldLocation.Y + d * i, fieldLocation.X + 12 * d, fieldLocation.Y + d * i);
            }
            b = false;
        }

        private void dora_Click(object sender, EventArgs e)
        {
            codeBox.Text += "Даша.";
            codeBox.SelectionStart = codeBox.Text.Length;
        }

        private void shoe_Click(object sender, EventArgs e)
        {
            codeBox.Text += "Башмачок.";
            codeBox.SelectionStart = codeBox.Text.Length;
        }

        private void backToMap_Click(object sender, EventArgs e)
        {
            int mapWidth = 0;
            switch (locationNow)
            {
                case 1:
                    mapWidth = 1065;
                    break;
                case 2:
                    mapWidth = 746;
                    break;
                case 3:
                    mapWidth = 833;
                    break;
                case 4:
                    mapWidth = 728;
                    break;
                case 5:
                    mapWidth = 719;
                    break;
                case 6:
                    mapWidth = 998;
                    break;
                case 7:
                    mapWidth = 883;
                    break;
                case 8:
                    mapWidth = 653;
                    break;
                case 9:
                    mapWidth = 764;
                    break;
                case 10:
                    mapWidth = 880;
                    break;
            }
            walkingTimer.Enabled = false;
            this.Hide();
            Map m = new Map(locationNow, levelNow, mapWidth);
            m.Width = mapWidth;
            m.Show();
        }
    }
}
