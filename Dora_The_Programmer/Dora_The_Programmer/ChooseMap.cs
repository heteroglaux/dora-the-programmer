﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Dora_The_Programmer
{
    public partial class ChooseMap : Form
    {
        Label[] locations;
        public ChooseMap()
        {
            int nextLoc = 1;
            using (StreamReader reader = new StreamReader("locationInfo"))
            {
                try
                {
                    nextLoc = Convert.ToInt32(reader.ReadLine());
                }
                catch (FormatException)
                {
                    MessageBox.Show("Файл, необходимый для работы программы, был поврежден! К сожалению, весь прогресс был утерян.");
                }
            }
            InitializeComponent();
            locations = new Label[] { label1, label2, label3, label4, label5, label6, label7, label8, label9, label10 };
            label1.Text += "\n(цветы)";
            label2.Text += "\n(снеговики)";
            label3.Text += "\n(кактусы)";
            label4.Text += "\n(крабики)";
            label5.Text += "\n(пингвины)";
            label6.Text += "\n(утята)";
            label7.Text += "\n(листья)";
            label8.Text += "\n(лилии)";
            label9.Text += "\n(зайцы)";
            label10.Text += "\n(облака)";
            for (int i = 0; i < nextLoc; i++)
                locations[i].Visible = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int mapWidth = 0;
            switch (comboBox1.SelectedIndex + 1)
            {
                case 1:
                    mapWidth = 1065;
                    break;
                case 2:
                    mapWidth = 746;
                    break;
                case 3:
                    mapWidth = 833;
                    break;
                case 4:
                    mapWidth = 728;
                    break;
                case 5:
                    mapWidth = 719;
                    break;
                case 6:
                    mapWidth = 998;
                    break;
                case 7:
                    mapWidth = 883;
                    break;
                case 8:
                    mapWidth = 653;
                    break;
                case 9:
                    mapWidth = 764;
                    break;
                case 10:
                    mapWidth = 880;
                    break;
            }
            this.Hide();
            new Rules(comboBox1.SelectedIndex, 0).ShowDialog();
            Map m = new Map(comboBox1.SelectedIndex + 1, 1, mapWidth);
            m.Width = mapWidth;
            m.Show();
        }

        private void label10_Click(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = Array.IndexOf(locations, sender);
        }

        private void ChooseMap_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void GO_Click(object sender, EventArgs e)
        {
            using (StreamWriter writer = new StreamWriter("locationInfo", false))
            {
                writer.Write("1");
            }
            for (int i = 1; i < locations.Length; i++)
                locations[i].Visible = false;
        }
    }
}
