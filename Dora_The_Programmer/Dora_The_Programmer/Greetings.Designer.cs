﻿namespace Dora_The_Programmer
{
    partial class Greetings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Greetings));
            this.textLabel = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.textTimer = new System.Windows.Forms.Timer(this.components);
            this.star1 = new System.Windows.Forms.PictureBox();
            this.star2 = new System.Windows.Forms.PictureBox();
            this.star3 = new System.Windows.Forms.PictureBox();
            this.starTimer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.star1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.star2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.star3)).BeginInit();
            this.SuspendLayout();
            // 
            // textLabel
            // 
            this.textLabel.BackColor = System.Drawing.Color.Transparent;
            this.textLabel.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textLabel.ForeColor = System.Drawing.Color.Brown;
            this.textLabel.Location = new System.Drawing.Point(150, 110);
            this.textLabel.Name = "textLabel";
            this.textLabel.Size = new System.Drawing.Size(438, 245);
            this.textLabel.TabIndex = 0;
            this.textLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.textLabel.Click += new System.EventHandler(this.textLabel_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Comic Sans MS", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.ForeColor = System.Drawing.Color.Maroon;
            this.button1.Location = new System.Drawing.Point(324, 303);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(147, 64);
            this.button1.TabIndex = 1;
            this.button1.Text = "Let\'s go!";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textTimer
            // 
            this.textTimer.Enabled = true;
            this.textTimer.Interval = 40;
            this.textTimer.Tick += new System.EventHandler(this.textTimer_Tick);
            // 
            // star1
            // 
            this.star1.BackColor = System.Drawing.Color.White;
            this.star1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("star1.BackgroundImage")));
            this.star1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.star1.Location = new System.Drawing.Point(297, 150);
            this.star1.Name = "star1";
            this.star1.Size = new System.Drawing.Size(65, 65);
            this.star1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.star1.TabIndex = 2;
            this.star1.TabStop = false;
            this.star1.Visible = false;
            // 
            // star2
            // 
            this.star2.BackColor = System.Drawing.Color.White;
            this.star2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("star2.BackgroundImage")));
            this.star2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.star2.Location = new System.Drawing.Point(368, 134);
            this.star2.Name = "star2";
            this.star2.Size = new System.Drawing.Size(65, 65);
            this.star2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.star2.TabIndex = 3;
            this.star2.TabStop = false;
            this.star2.Visible = false;
            // 
            // star3
            // 
            this.star3.BackColor = System.Drawing.Color.White;
            this.star3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("star3.BackgroundImage")));
            this.star3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.star3.Location = new System.Drawing.Point(439, 150);
            this.star3.Name = "star3";
            this.star3.Size = new System.Drawing.Size(65, 65);
            this.star3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.star3.TabIndex = 4;
            this.star3.TabStop = false;
            this.star3.Visible = false;
            // 
            // starTimer
            // 
            this.starTimer.Interval = 20;
            this.starTimer.Tick += new System.EventHandler(this.starTimer_Tick);
            // 
            // Greetings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Cornsilk;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(628, 419);
            this.Controls.Add(this.star3);
            this.Controls.Add(this.star2);
            this.Controls.Add(this.star1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textLabel);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(644, 457);
            this.MinimumSize = new System.Drawing.Size(644, 457);
            this.Name = "Greetings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Даша-Программист";
            this.Click += new System.EventHandler(this.Greetings_Click);
            ((System.ComponentModel.ISupportInitialize)(this.star1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.star2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.star3)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label textLabel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer textTimer;
        private System.Windows.Forms.PictureBox star1;
        private System.Windows.Forms.PictureBox star2;
        private System.Windows.Forms.PictureBox star3;
        private System.Windows.Forms.Timer starTimer;
    }
}