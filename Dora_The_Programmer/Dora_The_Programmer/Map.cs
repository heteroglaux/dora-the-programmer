﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dora_The_Programmer
{
    public partial class Map : Form
    {
        int currentLevel, currentLocation;
        int growingPicBox = 0;
        float startWidth, startHeight;
        Graphics g;
        Color[] backColors = {
                                 Color.FromArgb(233, 222, 150),
                                 Color.FromArgb(243, 254, 253),
                                 Color.FromArgb(248, 177, 50), 
                                 Color.FromArgb(250, 197, 52),
                                 Color.FromArgb(243, 254, 253),
                                 Color.FromArgb(248, 177, 50), 
                                 Color.FromArgb(254, 208, 91), 
                                 Color.FromArgb(153, 217, 234),
                                 Color.FromArgb(248, 177, 50), 
                                 Color.FromArgb(204, 228, 25) 
                             };
        Image[] levelPics;
        Image[] levelPointedPics;
        PictureBox[] picBoxes;

        public Map(int location, int level, int width)
        {
            InitializeComponent();
            currentLevel = level;
            currentLocation = location;

            picBoxes = new PictureBox[] { level1, level2, level3, level4, level5 };
            string path = "loc" + location + "/levelpics/";
            levelPics = new Image[] { Image.FromFile(path + "l1.png"), Image.FromFile(path + "l2.png"), Image.FromFile(path + "l3.png"), Image.FromFile(path + "l4.png"), Image.FromFile(path + "l5.png") };
            levelPointedPics = new Image[] { Image.FromFile(path + "lp1.png"), Image.FromFile(path + "lp2.png"), Image.FromFile(path + "lp3.png"), Image.FromFile(path + "lp4.png"), Image.FromFile(path + "lp5.png") };
            this.BackgroundImage = Image.FromFile("loc" + location + "/map.jpg");
            toolTip1.SetToolTip(mapBackButton, "Вернуться к главному меню!");

            switch (location)
            {
                case 1:
                    foreach (PictureBox pb in picBoxes)
                        pb.Size = new Size(95, 95);
                    level1.Location = new Point(146, 418);
                    level2.Location = new Point(310, 357);
                    level3.Location = new Point(481, 346);
                    level4.Location = new Point(661, 335);
                    level5.Location = new Point(828, 302);
                    mapBackButton.Location = new Point(33, 16);
                    mapBackButton.Size = new Size(100, 150);
                    break;
                case 2:
                    foreach (PictureBox pb in picBoxes)
                        pb.Size = new Size(95, 125);
                    level1.Location = new Point(1 * width / 6 - level1.Width / 2, 330);
                    level2.Location = new Point(2 * width / 6 - level1.Width / 2, 265);
                    level3.Location = new Point(3 * width / 6 - level1.Width / 2, 370);
                    level4.Location = new Point(4 * width / 6 - level1.Width / 2, 280);
                    level5.Location = new Point(5 * width / 6 - level1.Width / 2, 302);
                    mapBackButton.Location = new Point(605, 5);
                    mapBackButton.Size = new Size(100, 170);
                    break;
                case 3:
                    foreach (PictureBox pb in picBoxes)
                        pb.Size = new Size(150, 200);
                    level1.Location = new Point(1 * width / 6 - level1.Width / 2, 205);
                    level2.Location = new Point(2 * width / 6 - level1.Width / 2, 190);
                    level3.Location = new Point(3 * width / 6 - level1.Width / 2, 280);
                    level4.Location = new Point(4 * width / 6 - level1.Width / 2, 260);
                    level5.Location = new Point(5 * width / 6 - level1.Width / 2, 302);
                    mapBackButton.Location = new Point(0, 0);
                    mapBackButton.Size = new Size(100, 170);
                    break;
                case 4:
                    foreach (PictureBox pb in picBoxes)
                        pb.Size = new Size(100, 100);
                    level1.Location = new Point(1 * width / 6 - level1.Width / 2, 410);
                    level2.Location = new Point(2 * width / 6 - level1.Width / 2, 395);
                    level3.Location = new Point(3 * width / 6 - level1.Width / 2, 390);
                    level4.Location = new Point(4 * width / 6 - level1.Width / 2, 370);
                    level5.Location = new Point(5 * width / 6 - level1.Width / 2, 350);
                    mapBackButton.Location = new Point(30, 0);
                    mapBackButton.Size = new Size(100, 170);
                    break;
                case 5:
                    foreach (PictureBox pb in picBoxes)
                        pb.Size = new Size(70, 110);
                    level1.Location = new Point(1 * width / 6 - level1.Width / 2, 205);
                    level2.Location = new Point(2 * width / 6 - level1.Width / 2, 240);
                    level3.Location = new Point(3 * width / 6 - level1.Width / 2, 260);
                    level4.Location = new Point(4 * width / 6 - level1.Width / 2, 237);
                    level5.Location = new Point(5 * width / 6 - level1.Width / 2, 67);
                    mapBackButton.Location = new Point(0, 0);
                    mapBackButton.Size = new Size(90, 120);
                    break;
                case 6:
                    foreach (PictureBox pb in picBoxes)
                        pb.Size = new Size(90, 125);
                    level1.Location = new Point(1 * width / 6 - level1.Width / 2, 340);
                    level2.Location = new Point(2 * width / 6 - level1.Width / 2, 273);
                    level3.Location = new Point(3 * width / 6 - level1.Width / 2, 280);
                    level4.Location = new Point(4 * width / 6 - level1.Width / 2, 285);
                    level5.Location = new Point(5 * width / 6 - level1.Width / 2, 350);
                    mapBackButton.Location = new Point(15, 0);
                    mapBackButton.Size = new Size(130, 220);
                    break;
                case 7:
                    foreach (PictureBox pb in picBoxes)
                        pb.Size = new Size(95, 95);
                    level1.Location = new Point(1 * width / 6 - level1.Width / 2, 340);
                    level2.Location = new Point(2 * width / 6 - level1.Width / 2, 273);
                    level3.Location = new Point(3 * width / 6 - level1.Width / 2, 250);
                    level4.Location = new Point(4 * width / 6 - level1.Width / 2, 120);
                    level5.Location = new Point(5 * width / 6 - level1.Width / 2, 200);
                    mapBackButton.Location = new Point(740, 10);
                    mapBackButton.Size = new Size(120, 180);
                    break;
                case 8:
                    foreach (PictureBox pb in picBoxes)
                        pb.Size = new Size(100, 80);
                    level1.Location = new Point(1 * width / 6 - level1.Width / 2, 215);
                    level2.Location = new Point(2 * width / 6 - level1.Width / 2, 237);
                    level3.Location = new Point(3 * width / 6 - level1.Width / 2, 190);
                    level4.Location = new Point(4 * width / 6 - level1.Width / 2, 220);
                    level5.Location = new Point(5 * width / 6 - level1.Width / 2, 202);
                    mapBackButton.Location = new Point(0, 0);
                    mapBackButton.Size = new Size(80, 140);
                    break;
                case 9:
                    foreach (PictureBox pb in picBoxes)
                        pb.Size = new Size(100, 100);
                    level1.Location = new Point(1 * width / 6 - level1.Width / 2, 325);
                    level2.Location = new Point(2 * width / 6 - level1.Width / 2, 370);
                    level3.Location = new Point(3 * width / 6 - level1.Width / 2, 360);
                    level4.Location = new Point(4 * width / 6 - level1.Width / 2, 430);
                    level5.Location = new Point(5 * width / 6 - level1.Width / 2, 380);
                    mapBackButton.Location = new Point(75, 0);
                    mapBackButton.Size = new Size(100, 160);
                    break;
                case 10:
                    foreach (PictureBox pb in picBoxes)
                        pb.Size = new Size(140, 70);
                    level1.Location = new Point(1 * width / 6 - level1.Width / 2, 180);
                    level2.Location = new Point(2 * width / 6 - level1.Width / 2, 90);
                    level3.Location = new Point(3 * width / 6 - level1.Width / 2, 230);
                    level4.Location = new Point(4 * width / 6 - level1.Width / 2, 140);
                    level5.Location = new Point(5 * width / 6 - level1.Width / 2, 220);
                    mapBackButton.Location = new Point(750, 5);
                    mapBackButton.Size = new Size(100, 180);
                    break;
            }
            startWidth = picBoxes[level - 1].Width / 2;
            startHeight = picBoxes[level - 1].Height / 2;
            switch (level)
            {
                case 5:
                    level1.MouseMove += new MouseEventHandler(this.pictureBox1_MouseMove);
                    level2.MouseMove += new MouseEventHandler(this.pictureBox2_MouseMove);
                    level3.MouseMove += new MouseEventHandler(this.pictureBox3_MouseMove);
                    level4.MouseMove += new MouseEventHandler(this.pictureBox4_MouseMove);
                    level5.MouseMove += new MouseEventHandler(this.pictureBox5_MouseMove);
                    level1.MouseLeave += new EventHandler(this.pictureBox1_MouseLeave);
                    level2.MouseLeave += new EventHandler(this.pictureBox2_MouseLeave);
                    level3.MouseLeave += new EventHandler(this.pictureBox3_MouseLeave);
                    level4.MouseLeave += new EventHandler(this.pictureBox4_MouseLeave);
                    level5.MouseLeave += new EventHandler(this.pictureBox5_MouseLeave);
                    level1.Click += new EventHandler(this.pictureBox1_Click);
                    level2.Click += new EventHandler(this.pictureBox2_Click);
                    level3.Click += new EventHandler(this.pictureBox3_Click);
                    level4.Click += new EventHandler(this.pictureBox4_Click);
                    level5.Click += new EventHandler(this.pictureBox5_Click);
                    break;
                case 4:
                    level1.MouseMove += new MouseEventHandler(this.pictureBox1_MouseMove);
                    level2.MouseMove += new MouseEventHandler(this.pictureBox2_MouseMove);
                    level3.MouseMove += new MouseEventHandler(this.pictureBox3_MouseMove);
                    level4.MouseMove += new MouseEventHandler(this.pictureBox4_MouseMove);
                    level1.MouseLeave += new EventHandler(this.pictureBox1_MouseLeave);
                    level2.MouseLeave += new EventHandler(this.pictureBox2_MouseLeave);
                    level3.MouseLeave += new EventHandler(this.pictureBox3_MouseLeave);
                    level4.MouseLeave += new EventHandler(this.pictureBox4_MouseLeave);
                    level1.Click += new EventHandler(this.pictureBox1_Click);
                    level2.Click += new EventHandler(this.pictureBox2_Click);
                    level3.Click += new EventHandler(this.pictureBox3_Click);
                    level4.Click += new EventHandler(this.pictureBox4_Click);
                    break;
                case 3:
                    level1.MouseMove += new MouseEventHandler(this.pictureBox1_MouseMove);
                    level2.MouseMove += new MouseEventHandler(this.pictureBox2_MouseMove);
                    level3.MouseMove += new MouseEventHandler(this.pictureBox3_MouseMove);
                    level1.MouseLeave += new EventHandler(this.pictureBox1_MouseLeave);
                    level2.MouseLeave += new EventHandler(this.pictureBox2_MouseLeave);
                    level3.MouseLeave += new EventHandler(this.pictureBox3_MouseLeave);
                    level1.Click += new EventHandler(this.pictureBox1_Click);
                    level2.Click += new EventHandler(this.pictureBox2_Click);
                    level3.Click += new EventHandler(this.pictureBox3_Click);
                    break;
                case 2:
                    level1.MouseMove += new MouseEventHandler(this.pictureBox1_MouseMove);
                    level2.MouseMove += new MouseEventHandler(this.pictureBox2_MouseMove);
                    level1.MouseLeave += new EventHandler(this.pictureBox1_MouseLeave);
                    level2.MouseLeave += new EventHandler(this.pictureBox2_MouseLeave);
                    level1.Click += new EventHandler(this.pictureBox1_Click);
                    level2.Click += new EventHandler(this.pictureBox2_Click);
                    break;
                case 1:
                    level1.MouseMove += new MouseEventHandler(this.pictureBox1_MouseMove);
                    level1.MouseLeave += new EventHandler(this.pictureBox1_MouseLeave);
                    level1.Click += new EventHandler(this.pictureBox1_Click);
                    break;
            }
        }

        private void Map_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void flowerTimer_Tick(object sender, EventArgs e)
        {
            if (growingPicBox == currentLevel)
            {
                flowerTimer.Enabled = false;
                startWidth = picBoxes[0].Width;
                startHeight = picBoxes[0].Height;
            }
            else
            {
                g = picBoxes[growingPicBox].CreateGraphics();
                g.DrawImage(levelPics[growingPicBox], picBoxes[growingPicBox].Width / 2 - startWidth / 2, picBoxes[growingPicBox].Width / 2 - startWidth / 2, startWidth += 2, startHeight += (2 * (float)picBoxes[growingPicBox].Height / picBoxes[growingPicBox].Width));
                if (startWidth > picBoxes[growingPicBox].Width - 2)
                {
                    growingPicBox++;
                    startWidth = picBoxes[currentLevel - 1].Width / 2;
                    startHeight = picBoxes[currentLevel - 1].Height / 2;
                }
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            g = level1.CreateGraphics();
            g.DrawImage(levelPointedPics[0], 0, 0, picBoxes[0].Width, picBoxes[0].Height);
        }

        private void pictureBox1_MouseLeave(object sender, EventArgs e)
        {
            g = level1.CreateGraphics();
            g.DrawImage(levelPics[0], 0, 0, picBoxes[0].Width, picBoxes[0].Height);
        }

        private void pictureBox2_MouseMove(object sender, MouseEventArgs e)
        {
            g = level2.CreateGraphics();
            g.DrawImage(levelPointedPics[1], 0, 0, picBoxes[0].Width, picBoxes[0].Height);
        }

        private void pictureBox2_MouseLeave(object sender, EventArgs e)
        {
            g = level2.CreateGraphics();
            g.DrawImage(levelPics[1], 0, 0, picBoxes[0].Width, picBoxes[0].Height);
        }

        private void pictureBox3_MouseLeave(object sender, EventArgs e)
        {
            g = level3.CreateGraphics();
            g.DrawImage(levelPics[2], 0, 0, picBoxes[0].Width, picBoxes[0].Height);
        }

        private void pictureBox3_MouseMove(object sender, MouseEventArgs e)
        {
            g = level3.CreateGraphics();
            g.DrawImage(levelPointedPics[2], 0, 0, picBoxes[0].Width, picBoxes[0].Height);
        }

        private void pictureBox4_MouseLeave(object sender, EventArgs e)
        {
            g = level4.CreateGraphics();
            g.DrawImage(levelPics[3], 0, 0, picBoxes[0].Width, picBoxes[0].Height);
        }

        private void pictureBox4_MouseMove(object sender, MouseEventArgs e)
        {
            g = level4.CreateGraphics();
            g.DrawImage(levelPointedPics[3], 0, 0, picBoxes[0].Width, picBoxes[0].Height);
        }

        private void pictureBox5_MouseLeave(object sender, EventArgs e)
        {
            g = level5.CreateGraphics();
            g.DrawImage(levelPics[4], 0, 0, picBoxes[0].Width, picBoxes[0].Height);
        }

        private void pictureBox5_MouseMove(object sender, MouseEventArgs e)
        {
            g = level5.CreateGraphics();
            g.DrawImage(levelPointedPics[4], 0, 0, picBoxes[0].Width, picBoxes[0].Height);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            Form1 f = new Form1(currentLocation, 1);
            f.BackColor = backColors[currentLocation - 1];
            f.Show();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            Form1 f = new Form1(currentLocation, 2);
            f.BackColor = backColors[currentLocation - 1];
            f.Show();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            if (currentLocation == 3)
                new Rules(currentLocation - 1, 2).ShowDialog();
            Form1 f = new Form1(currentLocation, 3);
            f.BackColor = backColors[currentLocation - 1];
            f.Show();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            Form1 f = new Form1(currentLocation, 4);
            f.BackColor = backColors[currentLocation - 1];
            f.Show();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            ActiveForm.Hide();
            Form1 f = new Form1(currentLocation, 5);
            f.BackColor = backColors[currentLocation - 1];
            f.Show();
        }

        private void Map_Shown(object sender, EventArgs e)
        {
            flowerTimer.Enabled = true;
        }

        private void mapBackButton_MouseClick(object sender, MouseEventArgs e)
        {
            this.Hide();
            new ChooseMap().Show();
        }

        private void mapBackButton_MouseMove(object sender, MouseEventArgs e)
        {
            Cursor.Current = Cursors.Hand;
        }

        private void Map_Load(object sender, EventArgs e)
        {

        }
    }
}
