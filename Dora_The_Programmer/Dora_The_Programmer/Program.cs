﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Dora_The_Programmer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Greetings("greeting", "\n\nПривет, я Даша!\nЯ, как и ты, хочу стать настоящим программистом. Но мой ноутбук у Башмачка, а он куда-то убежал!.. \nПомоги мне найти его, и вместе мы станем настоящими специалистами!"));
        }
    }
}
