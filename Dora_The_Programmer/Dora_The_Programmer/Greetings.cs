﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Dora_The_Programmer
{
    public partial class Greetings : Form
    {
        int complitedLevel, curLocation;
        int star = 0;
        int gotStars;
        string text, type;
        Graphics g;
        PictureBox[] starsAr;
        public Greetings(string gtype, string gtext, int location = 1, int level = 0, int stars = 3)
        {
            InitializeComponent();

            starsAr = new PictureBox[] { star1, star2, star3 };
            switch (gtype)
            {
                case "greeting":
                    this.Text = "Даша-Программист";
                    this.BackgroundImage = Image.FromFile(@"bgimages/text.jpg");
                    textLabel.Location = new Point(145, 80);
                    textLabel.Size = new Size(588 - 145, 355 - 80);
                    button1.Location = new Point(295, 324);
                    button1.Text = "Let's go!";
                    break;
                case "error":
                    this.Text = "О, ужас!";
                    Attributes.itemsInBackpack = 0;
                    this.BackgroundImage = Image.FromFile(@"bgimages/fox1.jpg");
                    textLabel.Location = new Point(139, 153);
                    textLabel.Size = new Size(581 - 153, 381 - 113);
                    button1.Location = new Point(268, 305);
                    button1.Text = "Исправить!";
                    break;
                case "congratulations":
                    this.Text = "Ура!";
                    this.BackgroundImage = Image.FromFile(@"bgimages/dm1.jpg");
                    textLabel.Location = new Point(221, 161);
                    textLabel.Size = new Size(577 - 200, 378 - 132);
                    button1.Location = new Point(324, 303);
                    button1.Text = "Карта!";
                    complitedLevel = level;
                    curLocation = location;
                    foreach (PictureBox st in starsAr)
                        st.Visible = true;
                    starTimer.Enabled = true;
                    break;
                case "problem":
                    this.Text = "Задание";
                    this.BackColor = Color.Aquamarine;
                    textLabel.Location = new Point(50, 50);
                    //    textLabel.Size = new Size(588 - 150, 355 - 110);
                    button1.Location = new Point(250, 305);
                    button1.Text = "Вперед!";
                    break;
            }
            type = gtype;
            text = gtext;
            gotStars = stars;
        }

        int i = 0;
        private void textTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                textLabel.Text += text[i++];
            }
            catch
            {
                button1.Visible = true;
                textTimer.Enabled = false;
            }
        }

        private void Greetings_Click(object sender, EventArgs e)
        {
            textTimer.Enabled = false;
            textLabel.Text = text;
            button1.Visible = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            switch (type)
            {
                case "greeting":
                    this.Hide();
                    new ChooseMap().Show();
                    break;
                case "congratulations":
                    if (complitedLevel < 5)
                    {
                        int mapWidth = 0;
                        switch (curLocation)
                        {
                            case 1:
                                mapWidth = 1065;
                                break;
                            case 2:
                                mapWidth = 746;
                                break;
                            case 3:
                                mapWidth = 833;
                                break;
                            case 4:
                                mapWidth = 728;
                                break;
                            case 5:
                                mapWidth = 719;
                                break;
                            case 6:
                                mapWidth = 998;
                                break;
                            case 7:
                                mapWidth = 883;
                                break;
                            case 8:
                                mapWidth = 653;
                                break;
                            case 9:
                                mapWidth = 764;
                                break;
                            case 10:
                                mapWidth = 880;
                                break;
                        }
                        Map m = new Map(curLocation, complitedLevel + 1, mapWidth);
                        //Map m = new Map(curLocation, 5, mapWidth);
                        m.Width = mapWidth;
                        m.Show();
                    }
                    else
                        new ChooseMap().Show();
                    this.Hide();
                    break;
                default:
                    ActiveForm.Close();
                    break;
            }
        }

        private void textLabel_Click(object sender, EventArgs e)
        {
            textTimer.Enabled = false;
            textLabel.Text = text;
            button1.Visible = true;
        }

        int margin = 30, size = 5;
        private void starTimer_Tick(object sender, EventArgs e)
        {
            if (gotStars > star)
            {
                g = starsAr[star].CreateGraphics();
                size += 2;
                g.DrawImage(Image.FromFile("bgimages/starLight.png"), --margin, margin, size, size);
                if (margin < 0)
                {
                    star++;
                    margin = 30;
                    size = 5;
                }
            }
            else
            {
                starTimer.Enabled = false;
                for (int i = 0; i < gotStars; i++)
                    starsAr[i].BackgroundImage = Image.FromFile("bgimages/starLight.png");
            }
        }
    }
}
